<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Sluggable\SlugOptions;
use Spatie\Sluggable\HasSlug;

/**
 * Class PostCategory
 * @package App\Models
 * @version March 15, 2021, 6:10 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $posts
 * @property string $name
 * @property string $definition
 * @property integer $parent_id
 * @property string $slug
 */
class PostCategory extends Model
{
    use SoftDeletes;

    use HasFactory;

    use HasSlug;

    public $table = 'post_category';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'definition',
        'parent_id',
        'slug',
        'order'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'definition' => 'string',
        'parent_id' => 'integer',
        'slug' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|max:255',
        'definition' => 'nullable|string|max:255',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable',
        'slug' => 'nullable|string|max:255'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function posts()
    {
        return $this->hasMany(\App\Models\Post::class, 'post_category_id')->latest();
    }

    public function parent()
    {
        return $this->belongsTo(\App\Models\PostCategory::class, 'parent_id');
    }

    public function parentWithTrashed()
    {
        return $this->belongsTo(\App\Models\PostCategory::class, 'parent_id')->withTrashed();
    }

    public function childs()
    {
        return $this->hasMany(\App\Models\PostCategory::class, 'parent_id')->orderBy('order');
    }

    public function childsWithTrashed()
    {
        return $this->childs()->withTrashed();
    }

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug')->doNotGenerateSlugsOnUpdate();
    }

    public function scopeFindBySlug($query, $slug)
    {
        return $query->where('slug',$slug)->get();
    }
}
