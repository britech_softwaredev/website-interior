<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

/**
 * Class CategoryInterior
 * @package App\Models
 * @version June 11, 2022, 3:14 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $interiors
 * @property string $title
 * @property string $title_eng
 * @property string $description
 * @property string $description_eng
 * @property string $slug
 */
class CategoryInterior extends Model
{
    use SoftDeletes;
    use HasSlug;

    use HasFactory;

    public $table = 'category_interior';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'title_eng',
        'description',
        'description_eng',
        'slug'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'title_eng' => 'string',
        'description' => 'string',
        'description_eng' => 'string',
        'slug' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'nullable|string|max:255',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug')->doNotGenerateSlugsOnUpdate();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function interiors()
    {
        return $this->hasMany(\App\Models\Interior::class, 'category_interior_id');
    }
}
