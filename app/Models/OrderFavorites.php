<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class OrderFavorites
 * @package App\Models
 * @version June 14, 2022, 1:38 am UTC
 *
 * @property \App\Models\User $users
 * @property \App\Models\Interior $interior
 * @property \App\Models\Product $product
 * @property integer $users_id
 * @property string $uuid
 * @property integer $product_id
 * @property integer $interior_id
 */
class OrderFavorites extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'order_favorites';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'users_id',
        'uuid',
        'product_id',
        'interior_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'users_id' => 'integer',
        'uuid' => 'string',
        'product_id' => 'integer',
        'interior_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
//        'users_id' => 'required',
//        'uuid' => 'nullable|string|max:255',
//        'created_at' => 'nullable',
//        'deleted_at' => 'nullable',
//        'updated_at' => 'nullable',
//        'product_id' => 'nullable',
//        'interior_id' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function users()
    {
        return $this->belongsTo(\App\Models\User::class, 'users_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function interior()
    {
        return $this->belongsTo(\App\Models\InteriorGaleri::class, 'interior_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function product()
    {
        return $this->belongsTo(\App\Models\Produk::class, 'product_id');
    }
}
