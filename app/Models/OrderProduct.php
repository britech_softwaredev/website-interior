<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class OrderProduct
 * @package App\Models
 * @version June 14, 2022, 1:47 am UTC
 *
 * @property \App\Models\Status $status
 * @property \App\Models\User $users
 * @property \Illuminate\Database\Eloquent\Collection $detailOrders
 * @property integer $users_id
 * @property string $description_status
 * @property integer $status_id
 * @property string $note_order
 */
class OrderProduct extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'order_product';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'users_id',
        'description_status',
        'status_id',
        'note_order'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'users_id' => 'integer',
        'description_status' => 'string',
        'status_id' => 'integer',
        'note_order' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
//        'created_at' => 'nullable',
//        'updated_at' => 'nullable',
//        'deleted_at' => 'nullable',
//        'users_id' => 'required',
//        'description_status' => 'nullable|string',
//        'status_id' => 'required|integer',
//        'note_order' => 'nullable|string'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function status()
    {
        return $this->belongsTo(\App\Models\StatusOrder::class, 'status_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function users()
    {
        return $this->belongsTo(\App\Models\User::class, 'users_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function detailOrders()
    {
        return $this->hasMany(\App\Models\DetailOrder::class, 'order_product_id');
    }

    public function pembayaran()
    {
        return $this->hasOne(\App\Models\BuktiPembayaran::class, 'order_product_id');
    }
}
