<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\Sluggable\SlugOptions;
use Spatie\Sluggable\HasSlug;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use CyrildeWit\EloquentViewable\InteractsWithViews;
use CyrildeWit\EloquentViewable\Contracts\Viewable;



/**
 * Class Post
 * @package App\Models
 * @version March 15, 2021, 6:10 am UTC
 *
 * @property \App\Models\User $usersCreated
 * @property \App\Models\User $usersUpdated
 * @property \App\Models\PostCategory $postCategory
 * @property string $title
 * @property string $content
 * @property integer $users_created_id
 * @property integer $users_updated_id
 * @property integer $post_category_id
 * @property string $slug
 */
class Post extends Model implements HasMedia,Viewable
{
    use SoftDeletes;
    use HasFactory;
    use HasSlug;
    use InteractsWithMedia;
    use InteractsWithViews;

    public $table = 'post';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    public function registerMediaConversions(Media $media = null): void
    {
        $this
            ->addMediaConversion('preview')
            ->fit(Manipulations::FIT_CROP, 300, 300)
            ->nonQueued();

        $this->addMediaConversion('thumb')
            ->width(368)
            ->height(232)
            ->sharpen(10);

        $this->addMediaConversion('cover')
            ->width(2400)
            ->height(1800);
    }

    protected $appends=['created_at_standard'];

    public $fillable = [
        'title',
        'content',
        'image_caption',
        'users_created_id',
        'users_updated_id',
        'post_category_id',
        'slug',
        'created_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'content' => 'string',
        'image_caption' => 'string',
        'users_created_id' => 'integer',
        'users_updated_id' => 'integer',
        'post_category_id' => 'integer',
        'slug' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required|string|max:255',
        'content' => 'required|string',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable',
        'post_category_id' => 'required',
        'slug' => 'nullable|string|max:255'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function usersCreated()
    {
        return $this->belongsTo(\App\Models\User::class, 'users_created_id');
    }

    //Untuk relasi pencarian datatable
    public function users_created()
    {
        return $this->belongsTo(\App\Models\User::class, 'users_created_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function usersUpdated()
    {
        return $this->belongsTo(\App\Models\User::class, 'users_updated_id');
    }

    //Untuk relasi pencarian datatable
    public function users_updated()
    {
        return $this->belongsTo(\App\Models\User::class, 'users_updated_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function postCategory()
    {
        return $this->belongsTo(\App\Models\PostCategory::class, 'post_category_id');
    }

    //Untuk relasi pencarian datatable
    public function post_category()
    {
        return $this->belongsTo(\App\Models\PostCategory::class, 'post_category_id');
    }

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug')->doNotGenerateSlugsOnUpdate();
    }

    public function scopeFindBySlug($query, $slug)
    {
        return $query->where('slug',$slug)->get();
    }

    public function getCreatedAtStandardAttribute(){
        return $this->created_at->format('d-m-Y');
    }
}
