<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

/**
 * Class KategoriProduk
 * @package App\Models
 * @version June 12, 2022, 11:52 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $products
 * @property string $title
 * @property string $title_eng
 * @property string $description
 * @property string $description_eng
 * @property string $slug
 */
class CategoryProduct extends Model
{
    use SoftDeletes;
    use HasSlug;

    use HasFactory;

    public $table = 'category_product';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'title_eng',
        'description',
        'description_eng',
        'slug'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'title_eng' => 'string',
        'description' => 'string',
        'description_eng' => 'string',
        'slug' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'nullable|string|max:255',
        'description' => 'nullable|string',
    ];

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug')->doNotGenerateSlugsOnUpdate();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function products()
    {
        return $this->hasMany(Produk::class, 'category_product_id');
    }

    public function getKomersilsAttribute() {
//        return $this->products()->get();
        return $this->products()->where('komersil','1')->where('status',1)->get();
    }
}
