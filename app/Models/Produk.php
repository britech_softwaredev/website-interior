<?php

namespace App\Models;

use CyrildeWit\EloquentViewable\Contracts\Viewable;
use CyrildeWit\EloquentViewable\InteractsWithViews;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

/**
 * Class Produk
 * @package App\Models
 * @version June 12, 2022, 11:53 pm UTC
 *
 * @property \App\Models\CategoryProduct $categoryProduct
 * @property \Illuminate\Database\Eloquent\Collection $interiors
 * @property \Illuminate\Database\Eloquent\Collection $orderFavorites
 * @property string $title
 * @property string $title_eng
 * @property string $description
 * @property string $description_eng
 * @property integer $price
 * @property string $color
 * @property string $material
 * @property string $dimensions
 * @property string $produsen
 * @property string $slug
 * @property integer $category_product_id
 */
class Produk extends Model implements HasMedia,Viewable
{
    use SoftDeletes;
    use HasSlug;

    use HasFactory;
    use InteractsWithMedia;
    use InteractsWithViews;

    public $table = 'product';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    public function registerMediaConversions(Media $media = null): void
    {
        $this
            ->addMediaConversion('preview')
            ->fit(Manipulations::FIT_CROP, 300, 300)
            ->nonQueued();

        $this->addMediaConversion('thumb')
            ->width(368)
            ->height(232)
            ->sharpen(10);

        $this->addMediaConversion('cover')
            ->width(2400)
            ->height(1800);
    }

    public $fillable = [
        'title',
        'title_eng',
        'description',
        'description_eng',
        'price',
        'color',
        'material',
        'dimensions',
        'produsen',
        'slug',
        'komersil',
        'status',
        'category_product_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'title_eng' => 'string',
        'description' => 'string',
        'description_eng' => 'string',
        'price' => 'integer',
        'color' => 'string',
        'material' => 'string',
        'dimensions' => 'string',
        'produsen' => 'string',
        'slug' => 'string',
        'komersil' => 'integer',
        'status' => 'integer',
        'category_product_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'nullable|string|max:255',
//        'title_eng' => 'nullable|string|max:255',
//        'description' => 'nullable|string',
//        'description_eng' => 'nullable|string',
//        'price' => 'nullable',
//        'color' => 'nullable|string|max:255',
//        'material' => 'nullable|string|max:255',
//        'dimensions' => 'nullable|string|max:255',
//        'produsen' => 'nullable|string|max:255',
        'category_product_id' => 'required|integer'
    ];

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug')->doNotGenerateSlugsOnUpdate();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function categoryProduct()
    {
        return $this->belongsTo(\App\Models\CategoryProduct::class, 'category_product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function interiors()
    {
        return $this->belongsToMany(\App\Models\Interior::class, 'interior_has_product');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function orderFavorites()
    {
        return $this->hasMany(\App\Models\OrderFavorite::class, 'product_id');
    }
}
