<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class InteriorHasProduct
 * @package App\Models
 * @version June 13, 2022, 11:16 am UTC
 *
 * @property \App\Models\Interior $interior
 * @property \App\Models\Product $product
 * @property integer $product_id
 */
class InteriorHasProduct extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'interior_has_product';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    public $fillable = [
        'product_id',
        'interior_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'interior_id' => 'integer',
        'product_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
//        'product_id' => 'required',
//        'created_at' => 'nullable',
//        'updated_at' => 'nullable',
//        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function interior()
    {
        return $this->belongsTo(\App\Models\InteriorGaleri::class, 'interior_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function product()
    {
        return $this->belongsTo(\App\Models\Produk::class, 'product_id');
    }
}
