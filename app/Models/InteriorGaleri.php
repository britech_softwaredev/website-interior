<?php

namespace App\Models;

use CyrildeWit\EloquentViewable\Contracts\Viewable;
use CyrildeWit\EloquentViewable\InteractsWithViews;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

/**
 * Class InteriorGaleri
 * @package App\Models
 * @version June 11, 2022, 3:10 am UTC
 *
 * @property \App\Models\CategoryInterior $categoryInterior
 * @property \Illuminate\Database\Eloquent\Collection $detailInteriors
 * @property \Illuminate\Database\Eloquent\Collection $products
 * @property \Illuminate\Database\Eloquent\Collection $orderFavorites
 * @property string $title
 * @property string $title_eng
 * @property string $description
 * @property string $description_eng
 * @property integer $price
 * @property string $dimensions
 * @property string $slug
 * @property integer $category_interior_id
 */
class InteriorGaleri extends Model implements HasMedia,Viewable
{
    use SoftDeletes;
    use HasSlug;

    use HasFactory;
    use InteractsWithMedia;
    use InteractsWithViews;

    public $table = 'interior';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    public function registerMediaConversions(Media $media = null): void
    {
        $this
            ->addMediaConversion('preview')
            ->fit(Manipulations::FIT_CROP, 300, 300)
            ->nonQueued();

        $this->addMediaConversion('thumb')
            ->width(368)
            ->height(232)
            ->sharpen(10);

        $this->addMediaConversion('cover')
            ->width(2400)
            ->height(1800);
    }

    public $fillable = [
        'title',
        'title_eng',
        'description',
        'description_eng',
        'price',
        'dimensions',
        'slug',
        'category_interior_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'title_eng' => 'string',
        'description' => 'string',
        'description_eng' => 'string',
        'price' => 'integer',
        'dimensions' => 'string',
        'slug' => 'string',
        'category_interior_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'nullable|string|max:255',
        'title_eng' => 'nullable|string|max:255',
//        'description' => 'nullable|string',
//        'description_eng' => 'nullable|string',
//        'price' => 'nullable',
//        'dimensions' => 'nullable|string|max:255',
//        'slug' => 'nullable|string|max:255',
//        'created_at' => 'required',
//        'updated_at' => 'nullable',
//        'deleted_at' => 'nullable',
//        'category_interior_id' => 'required|integer'
    ];

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug')->doNotGenerateSlugsOnUpdate();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function categoryInterior()
    {
        return $this->belongsTo(\App\Models\CategoryInterior::class, 'category_interior_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function detailInteriors()
    {
        return $this->hasMany(\App\Models\DetailInterior::class, 'interior_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function products()
    {
        return $this->belongsToMany(\App\Models\Produk::class, 'interior_has_product','interior_id','product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function orderFavorites()
    {
        return $this->hasMany(\App\Models\OrderFavorite::class, 'interior_id');
    }
}
