<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class DetailOrder
 * @package App\Models
 * @version June 14, 2022, 4:09 am UTC
 *
 * @property \App\Models\OrderProduct $orderProduct
 * @property \App\Models\Product $product
 * @property integer $product_id
 * @property integer $order_product_id
 */
class DetailOrder extends Model
{
//    use SoftDeletes;

    use HasFactory;

    public $table = 'detail_order';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

//    protected $dates = ['deleted_at'];

    public $fillable = [
        'product_id',
        'order_product_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'product_id' => 'integer',
        'order_product_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
//        'created_at' => 'nullable',
//        'updated_at' => 'nullable',
//        'deleted_at' => 'nullable',
//        'product_id' => 'required',
//        'order_product_id' => 'required|integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function orderProduct()
    {
        return $this->belongsTo(\App\Models\OrderProduct::class, 'order_product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function product()
    {
        return $this->belongsTo(\App\Models\Produk::class, 'product_id');
    }
}
