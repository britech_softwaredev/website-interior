<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Sluggable\SlugOptions;
use Spatie\Sluggable\HasSlug;

/**
 * Class PageCategory
 * @package App\Models
 * @version March 15, 2021, 6:08 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $pages
 * @property string $name
 * @property string $definition
 * @property integer $parent_id
 * @property string $slug
 */
class PageCategory extends Model
{
    use SoftDeletes;

    use HasFactory;

    use HasSlug;

    public $table = 'page_category';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'definition',
        'parent_id',
        'slug'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'definition' => 'string',
        'parent_id' => 'integer',
        'slug' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|max:255',
        'definition' => 'nullable|string|max:255',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable',
        'slug' => 'nullable|string|max:255'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function pages()
    {
        return $this->hasMany(\App\Models\Page::class, 'page_category_id');
    }

    public function parent()
    {
        return $this->belongsTo(\App\Models\PageCategory::class, 'parent_id');
    }

    public function childs()
    {
        return $this->hasMany(\App\Models\PageCategory::class, 'parent_id');
    }

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug')->doNotGenerateSlugsOnUpdate();
    }

    public function scopeFindBySlug($query, $slug)
    {
        return $query->where('slug',$slug)->get();
    }
}
