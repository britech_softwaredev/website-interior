<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePageRequest;
use App\Http\Requests\UpdatePageRequest;
use App\Models\Page;
use App\Repositories\PageRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Cache;
use Response;
use Auth;

class PageController extends AppBaseController
{
    /** @var  PageRepository */
    private $pageRepository;

    public function __construct(PageRepository $pageRepo)
    {
        $this->pageRepository = $pageRepo;
    }

    /**
     * Display a listing of the Page.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $this->middleware(['auth','permissions:pages.index']);
        $pages = Page::withTrashed()->orderBy('order')->get();

        return view('pages.index')
            ->with('pages', $pages);
    }

    /**
     * Show the form for creating a new Page.
     *
     * @return Response
     */
    public function create()
    {
        $this->middleware(['auth','permissions:pages.create']);
        $pageCategories= \App\Models\PageCategory::pluck('name','id');
        return view('pages.create',compact('pageCategories'));
    }

    /**
     * Store a newly created Page in storage.
     *
     * @param CreatePageRequest $request
     *
     * @return Response
     */
    public function store(CreatePageRequest $request)
    {
        $this->middleware(['auth','permissions:pages.store']);
        $input = $request->all();
        $input['users_created_id'] = Auth::user()->id;
        $input['users_updated_id'] = Auth::user()->id;

        $page = $this->pageRepository->create($input);

        $page->addFromMediaLibraryRequest($request->media)
            ->toMediaCollection();

        Flash::success('Page saved successfully.');

        return redirect(route('pages.index'));
    }

    /**
     * Display the specified Page.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $page = $this->pageRepository->find($id);

        if (empty($page)) {
            Flash::error('Page not found');

            return redirect(route('pages.index'));
        }

        return view('pages.show')->with('page', $page);
    }

    /**
     * Show the form for editing the specified Page.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->middleware(['auth','permissions:pages.edit']);
        $page = $this->pageRepository->find($id);

        if (empty($page)) {
            Flash::error('Page not found');

            return redirect(route('pages.index'));
        }
        $pageCategories= \App\Models\PageCategory::pluck('name','id');
        return view('pages.edit',compact('pageCategories'))->with('page', $page);
    }

    /**
     * Update the specified Page in storage.
     *
     * @param int $id
     * @param UpdatePageRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePageRequest $request)
    {
        $this->middleware(['auth','permissions:pages.update']);
        $page = $this->pageRepository->find($id);
        $input = $request->all();
        $input['users_updated_id'] = Auth::user()->id;

        if (empty($page)) {
            Flash::error('Page not found');

            return redirect(route('pages.index'));
        }

        $page->syncFromMediaLibraryRequest($request->media)
            ->toMediaCollection();

        $page = $this->pageRepository->update($input, $id);

        Cache::forget('detailPage-'.$page->slug);

        Flash::success('Page updated successfully.');

        return redirect(route('pages.index'));
    }

    /**
     * Remove the specified Page from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->middleware(['auth','permissions:pages.destroy']);
        $page = Page::withTrashed()->find($id);

        if (empty($page)) {
            Flash::error('Page not found');

            return redirect(route('pages.index'));
        }

        if(!$page->trashed()){
            $this->pageRepository->delete($id);
        }else{
            $page->restore();
        }

        Flash::success('Page deleted successfully.');

        return redirect(route('pages.index'));
    }

    public function increase($id)
    {
        $page = Page::withTrashed()->find($id);

        if (empty($page)) {
            Flash::error('Page not found');
            return redirect(route('pages.index'));
        }

        if($page->order>0){
            $page->order=$page->order-1;
            $page->save();
            Flash::success('Urutan Berhasil Naik');
        }else{
            Flash::error('Urutan mencapai nilai batas');
        }

        return redirect(route('pages.index'));
    }

    public function decrease($id)
    {
        $page = Page::withTrashed()->find($id);

        if (empty($page)) {
            Flash::error('Page not found');
            return redirect(route('pages.index'));
        }

        $page->order=$page->order+1;
        $page->save();
        Flash::success('Urutan Berhasil Turun');

        return redirect(route('pages.index'));
    }
}
