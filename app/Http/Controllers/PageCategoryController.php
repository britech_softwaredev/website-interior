<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePageCategoryRequest;
use App\Http\Requests\UpdatePageCategoryRequest;
use App\Models\PageCategory;
use App\Repositories\PageCategoryRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class PageCategoryController extends AppBaseController
{
    /** @var  PageCategoryRepository */
    private $pageCategoryRepository;

    public function __construct(PageCategoryRepository $pageCategoryRepo)
    {
        $this->pageCategoryRepository = $pageCategoryRepo;
    }

    /**
     * Display a listing of the PageCategory.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $this->middleware(['auth','permissions:pageCategories.index']);
        $pageCategories = PageCategory::withTrashed()->orderBy('order')->get();

        return view('page_categories.index')
            ->with('pageCategories', $pageCategories);
    }

    /**
     * Show the form for creating a new PageCategory.
     *
     * @return Response
     */
    public function create()
    {
        $this->middleware(['auth','permissions:pageCategories.create']);
        //$pageCategories= \App\Models\PageCategory::whereNull('parent_id')->pluck('name','id');
        return view('page_categories.create');
    }

    /**
     * Store a newly created PageCategory in storage.
     *
     * @param CreatePageCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreatePageCategoryRequest $request)
    {
        $this->middleware(['auth','permissions:pageCategories.store']);
        $input = $request->all();

        $pageCategory = $this->pageCategoryRepository->create($input);

        Flash::success('Page Category saved successfully.');

        return redirect(route('pageCategories.index'));
    }

    /**
     * Display the specified PageCategory.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pageCategory = $this->pageCategoryRepository->find($id);

        if (empty($pageCategory)) {
            Flash::error('Page Category not found');

            return redirect(route('pageCategories.index'));
        }

        return view('page_categories.show')->with('pageCategory', $pageCategory);
    }

    /**
     * Show the form for editing the specified PageCategory.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->middleware(['auth','permissions:pageCategories.edit']);
        $pageCategory = $this->pageCategoryRepository->find($id);

        if (empty($pageCategory)) {
            Flash::error('Page Category not found');

            return redirect(route('pageCategories.index'));
        }
        //$pageCategories= \App\Models\PageCategory::whereNull('parent_id')->pluck('name','id');
        return view('page_categories.edit')->with('pageCategory', $pageCategory);
    }

    /**
     * Update the specified PageCategory in storage.
     *
     * @param int $id
     * @param UpdatePageCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePageCategoryRequest $request)
    {
        $this->middleware(['auth','permissions:pageCategories.update']);
        $pageCategory = $this->pageCategoryRepository->find($id);

        if (empty($pageCategory)) {
            Flash::error('Page Category not found');

            return redirect(route('pageCategories.index'));
        }

        $pageCategory = $this->pageCategoryRepository->update($request->all(), $id);

        Flash::success('Page Category updated successfully.');

        return redirect(route('pageCategories.index'));
    }

    /**
     * Remove the specified PageCategory from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->middleware(['auth','permissions:pageCategories.destroy']);
        $pageCategory = PageCategory::withTrashed()->find($id);

        if (empty($pageCategory)) {
            Flash::error('Page Category not found');

            return redirect(route('pageCategories.index'));
        }

        if(!$pageCategory->trashed()){
            $this->pageCategoryRepository->delete($id);
        }else{
            $pageCategory->restore();
        }


        Flash::success('Page Category deleted successfully.');

        return redirect(route('pageCategories.index'));
    }

    public function increase($id)
    {
        $pageCategory = PageCategory::withTrashed()->find($id);

        if (empty($pageCategory)) {
            Flash::error('Page Category not found');
            return redirect(route('pageCategories.index'));
        }

        if($pageCategory->order>0){
            $pageCategory->order=$pageCategory->order-1;
            $pageCategory->save();
            Flash::success('Urutan Berhasil Naik');
        }else{
            Flash::error('Urutan mencapai nilai batas');
        }

        return redirect(route('pageCategories.index'));
    }

    public function decrease($id)
    {
        $pageCategory = PageCategory::withTrashed()->find($id);

        if (empty($pageCategory)) {
            Flash::error('Page Category not found');
            return redirect(route('pageCategories.index'));
        }

        $pageCategory->order=$pageCategory->order+1;
        $pageCategory->save();
        Flash::success('Urutan Berhasil Turun');

        return redirect(route('pageCategories.index'));
    }
}
