<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRekeningRequest;
use App\Http\Requests\UpdateRekeningRequest;
use App\Repositories\RekeningRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class RekeningController extends AppBaseController
{
    /** @var  RekeningRepository */
    private $rekeningRepository;

    public function __construct(RekeningRepository $rekeningRepo)
    {
        $this->rekeningRepository = $rekeningRepo;
    }

    /**
     * Display a listing of the Rekening.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $rekenings = $this->rekeningRepository->all();

        return view('rekenings.index')
            ->with('rekenings', $rekenings);
    }

    /**
     * Show the form for creating a new Rekening.
     *
     * @return Response
     */
    public function create()
    {
        return view('rekenings.create');
    }

    /**
     * Store a newly created Rekening in storage.
     *
     * @param CreateRekeningRequest $request
     *
     * @return Response
     */
    public function store(CreateRekeningRequest $request)
    {
        $input = $request->all();

        $rekening = $this->rekeningRepository->create($input);

        Flash::success('Rekening saved successfully.');

        return redirect(route('rekenings.index'));
    }

    /**
     * Display the specified Rekening.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $rekening = $this->rekeningRepository->find($id);

        if (empty($rekening)) {
            Flash::error('Rekening not found');

            return redirect(route('rekenings.index'));
        }

        return view('rekenings.show')->with('rekening', $rekening);
    }

    /**
     * Show the form for editing the specified Rekening.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $rekening = $this->rekeningRepository->find($id);

        if (empty($rekening)) {
            Flash::error('Rekening not found');

            return redirect(route('rekenings.index'));
        }

        return view('rekenings.edit')->with('rekening', $rekening);
    }

    /**
     * Update the specified Rekening in storage.
     *
     * @param int $id
     * @param UpdateRekeningRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRekeningRequest $request)
    {
        $rekening = $this->rekeningRepository->find($id);

        if (empty($rekening)) {
            Flash::error('Rekening not found');

            return redirect(route('rekenings.index'));
        }

        $rekening = $this->rekeningRepository->update($request->all(), $id);

        Flash::success('Rekening updated successfully.');

        return redirect(route('rekenings.index'));
    }

    /**
     * Remove the specified Rekening from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $rekening = $this->rekeningRepository->find($id);

        if (empty($rekening)) {
            Flash::error('Rekening not found');

            return redirect(route('rekenings.index'));
        }

        $this->rekeningRepository->delete($id);

        Flash::success('Rekening deleted successfully.');

        return redirect(route('rekenings.index'));
    }
}
