<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateInteriorHasProductRequest;
use App\Http\Requests\UpdateInteriorHasProductRequest;
use App\Repositories\InteriorHasProductRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class InteriorHasProductController extends AppBaseController
{
    /** @var  InteriorHasProductRepository */
    private $interiorHasProductRepository;

    public function __construct(InteriorHasProductRepository $interiorHasProductRepo)
    {
        $this->interiorHasProductRepository = $interiorHasProductRepo;
    }

    /**
     * Display a listing of the InteriorHasProduct.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $interiorHasProducts = $this->interiorHasProductRepository->all();

        return view('interior_has_products.index')
            ->with('interiorHasProducts', $interiorHasProducts);
    }

    /**
     * Show the form for creating a new InteriorHasProduct.
     *
     * @return Response
     */
    public function create()
    {
        return view('interior_has_products.create');
    }

    /**
     * Store a newly created InteriorHasProduct in storage.
     *
     * @param CreateInteriorHasProductRequest $request
     *
     * @return Response
     */
    public function store(CreateInteriorHasProductRequest $request)
    {
        $input = $request->all();

        $interiorHasProduct = $this->interiorHasProductRepository->create($input);

        Flash::success('Interior Has Product saved successfully.');

        return redirect(route('interiorHasProducts.index'));
    }

    /**
     * Display the specified InteriorHasProduct.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $interiorHasProduct = $this->interiorHasProductRepository->find($id);

        if (empty($interiorHasProduct)) {
            Flash::error('Interior Has Product not found');

            return redirect(route('interiorHasProducts.index'));
        }

        return view('interior_has_products.show')->with('interiorHasProduct', $interiorHasProduct);
    }

    /**
     * Show the form for editing the specified InteriorHasProduct.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $interiorHasProduct = $this->interiorHasProductRepository->find($id);

        if (empty($interiorHasProduct)) {
            Flash::error('Interior Has Product not found');

            return redirect(route('interiorHasProducts.index'));
        }

        return view('interior_has_products.edit')->with('interiorHasProduct', $interiorHasProduct);
    }

    /**
     * Update the specified InteriorHasProduct in storage.
     *
     * @param int $id
     * @param UpdateInteriorHasProductRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInteriorHasProductRequest $request)
    {
        $interiorHasProduct = $this->interiorHasProductRepository->find($id);

        if (empty($interiorHasProduct)) {
            Flash::error('Interior Has Product not found');

            return redirect(route('interiorHasProducts.index'));
        }

        $interiorHasProduct = $this->interiorHasProductRepository->update($request->all(), $id);

        Flash::success('Interior Has Product updated successfully.');

        return redirect(route('interiorHasProducts.index'));
    }

    /**
     * Remove the specified InteriorHasProduct from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $interiorHasProduct = $this->interiorHasProductRepository->find($id);

        if (empty($interiorHasProduct)) {
            Flash::error('Interior Has Product not found');

            return redirect(route('interiorHasProducts.index'));
        }

        $this->interiorHasProductRepository->delete($id);

        Flash::success('Interior Has Product deleted successfully.');

        return redirect(route('interiorHasProducts.index'));
    }
}
