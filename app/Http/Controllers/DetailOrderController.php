<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDetailOrderRequest;
use App\Http\Requests\UpdateDetailOrderRequest;
use App\Repositories\DetailOrderRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class DetailOrderController extends AppBaseController
{
    /** @var  DetailOrderRepository */
    private $detailOrderRepository;

    public function __construct(DetailOrderRepository $detailOrderRepo)
    {
        $this->detailOrderRepository = $detailOrderRepo;
    }

    /**
     * Display a listing of the DetailOrder.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $detailOrders = $this->detailOrderRepository->all();

        return view('detail_orders.index')
            ->with('detailOrders', $detailOrders);
    }

    /**
     * Show the form for creating a new DetailOrder.
     *
     * @return Response
     */
    public function create()
    {
        return view('detail_orders.create');
    }

    /**
     * Store a newly created DetailOrder in storage.
     *
     * @param CreateDetailOrderRequest $request
     *
     * @return Response
     */
    public function store(CreateDetailOrderRequest $request)
    {
        $input = $request->all();

        $detailOrder = $this->detailOrderRepository->create($input);

        Flash::success('Detail Order saved successfully.');

        return redirect(route('detailOrders.index'));
    }

    /**
     * Display the specified DetailOrder.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $detailOrder = $this->detailOrderRepository->find($id);

        if (empty($detailOrder)) {
            Flash::error('Detail Order not found');

            return redirect(route('detailOrders.index'));
        }

        return view('detail_orders.show')->with('detailOrder', $detailOrder);
    }

    /**
     * Show the form for editing the specified DetailOrder.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $detailOrder = $this->detailOrderRepository->find($id);

        if (empty($detailOrder)) {
            Flash::error('Detail Order not found');

            return redirect(route('detailOrders.index'));
        }

        return view('detail_orders.edit')->with('detailOrder', $detailOrder);
    }

    /**
     * Update the specified DetailOrder in storage.
     *
     * @param int $id
     * @param UpdateDetailOrderRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDetailOrderRequest $request)
    {
        $detailOrder = $this->detailOrderRepository->find($id);

        if (empty($detailOrder)) {
            Flash::error('Detail Order not found');

            return redirect(route('detailOrders.index'));
        }

        $detailOrder = $this->detailOrderRepository->update($request->all(), $id);

        Flash::success('Detail Order updated successfully.');

        return redirect(route('detailOrders.index'));
    }

    /**
     * Remove the specified DetailOrder from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $detailOrder = $this->detailOrderRepository->find($id);

        if (empty($detailOrder)) {
            Flash::error('Detail Order not found');

            return redirect(route('detailOrders.index'));
        }

        $this->detailOrderRepository->delete($id);

        Flash::success('Detail Order deleted successfully.');

        return redirect()->back();
    }
}
