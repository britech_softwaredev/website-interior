<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InstagramController extends Controller
{
    //
    public function instagram_auth_callback(){

    }

    public function complete() {
        $was_successful = request('result') === 'success';

        return view('instagram-auth-response-page', ['was_successful' => $was_successful]);
    }
}
