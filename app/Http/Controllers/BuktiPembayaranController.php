<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBuktiPembayaranRequest;
use App\Http\Requests\UpdateBuktiPembayaranRequest;
use App\Models\OrderProduct;
use App\Models\StatusOrder;
use App\Repositories\BuktiPembayaranRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class BuktiPembayaranController extends AppBaseController
{
    /** @var  BuktiPembayaranRepository */
    private $buktiPembayaranRepository;

    public function __construct(BuktiPembayaranRepository $buktiPembayaranRepo)
    {
        $this->buktiPembayaranRepository = $buktiPembayaranRepo;
    }

    /**
     * Display a listing of the BuktiPembayaran.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $buktiPembayarans = $this->buktiPembayaranRepository->all();

        return view('bukti_pembayarans.index')
            ->with('buktiPembayarans', $buktiPembayarans);
    }

    /**
     * Show the form for creating a new BuktiPembayaran.
     *
     * @return Response
     */
    public function create()
    {
        return view('bukti_pembayarans.create');
    }

    /**
     * Store a newly created BuktiPembayaran in storage.
     *
     * @param CreateBuktiPembayaranRequest $request
     *
     * @return Response
     */
    public function store(CreateBuktiPembayaranRequest $request)
    {
        $status = StatusOrder::where('name','like','%PROSES%')->first();
        $input = $request->all();

        $buktiPembayaran = $this->buktiPembayaranRepository->create($input);

        $buktiPembayaran->addFromMediaLibraryRequest($request->media)
            ->toMediaCollection();

        $order = OrderProduct::find($buktiPembayaran->order_product_id);
        $order['note_order'] = "Verifikasi Pembayaran";
        $order['status_id'] = $status->id;
        $order->update();

        Flash::success('Bukti Pembayaran saved successfully.');

        return redirect(url('orderStatus'));
    }

    /**
     * Display the specified BuktiPembayaran.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $buktiPembayaran = $this->buktiPembayaranRepository->find($id);

        if (empty($buktiPembayaran)) {
            Flash::error('Bukti Pembayaran not found');

            return redirect(route('buktiPembayarans.index'));
        }

        return view('bukti_pembayarans.show')->with('buktiPembayaran', $buktiPembayaran);
    }

    /**
     * Show the form for editing the specified BuktiPembayaran.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $buktiPembayaran = $this->buktiPembayaranRepository->find($id);

        if (empty($buktiPembayaran)) {
            Flash::error('Bukti Pembayaran not found');

            return redirect(route('buktiPembayarans.index'));
        }

        return view('bukti_pembayarans.edit')->with('buktiPembayaran', $buktiPembayaran);
    }

    /**
     * Update the specified BuktiPembayaran in storage.
     *
     * @param int $id
     * @param UpdateBuktiPembayaranRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBuktiPembayaranRequest $request)
    {
        $buktiPembayaran = $this->buktiPembayaranRepository->find($id);

        if (empty($buktiPembayaran)) {
            Flash::error('Bukti Pembayaran not found');

            return redirect(route('buktiPembayarans.index'));
        }

        $buktiPembayaran = $this->buktiPembayaranRepository->update($request->all(), $id);

        Flash::success('Bukti Pembayaran updated successfully.');

        return redirect(route('buktiPembayarans.index'));
    }

    /**
     * Remove the specified BuktiPembayaran from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $buktiPembayaran = $this->buktiPembayaranRepository->find($id);

        if (empty($buktiPembayaran)) {
            Flash::error('Bukti Pembayaran not found');

            return redirect(route('buktiPembayarans.index'));
        }

        $this->buktiPembayaranRepository->delete($id);

        Flash::success('Bukti Pembayaran deleted successfully.');

        return redirect(route('buktiPembayarans.index'));
    }
}
