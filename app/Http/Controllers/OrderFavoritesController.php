<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrderFavoritesRequest;
use App\Http\Requests\UpdateOrderFavoritesRequest;
use App\Models\OrderFavorites;
use App\Repositories\OrderFavoritesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use Auth;

class OrderFavoritesController extends AppBaseController
{
    /** @var  OrderFavoritesRepository */
    private $orderFavoritesRepository;

    public function __construct(OrderFavoritesRepository $orderFavoritesRepo)
    {
        $this->orderFavoritesRepository = $orderFavoritesRepo;
    }

    /**
     * Display a listing of the OrderFavorites.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $orderFavorites = $this->orderFavoritesRepository->all();

        return view('order_favorites.index')
            ->with('orderFavorites', $orderFavorites);
    }

    /**
     * Show the form for creating a new OrderFavorites.
     *
     * @return Response
     */
    public function create()
    {
        return view('order_favorites.create');
    }

    /**
     * Store a newly created OrderFavorites in storage.
     *
     * @param CreateOrderFavoritesRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderFavoritesRequest $request)
    {
        $input = $request->all();

        $orderFavorites = $this->orderFavoritesRepository->create($input);

        Flash::success('Order Favorites saved successfully.');

        return redirect(route('orderFavorites.index'));
    }

    /**
     * Display the specified OrderFavorites.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $orderFavorites = $this->orderFavoritesRepository->find($id);

        if (empty($orderFavorites)) {
            Flash::error('Order Favorites not found');

            return redirect(route('orderFavorites.index'));
        }

        return view('order_favorites.show')->with('orderFavorites', $orderFavorites);
    }

    /**
     * Show the form for editing the specified OrderFavorites.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $orderFavorites = $this->orderFavoritesRepository->find($id);

        if (empty($orderFavorites)) {
            Flash::error('Order Favorites not found');

            return redirect(route('orderFavorites.index'));
        }

        return view('order_favorites.edit')->with('orderFavorites', $orderFavorites);
    }

    /**
     * Update the specified OrderFavorites in storage.
     *
     * @param int $id
     * @param UpdateOrderFavoritesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderFavoritesRequest $request)
    {
        $orderFavorites = $this->orderFavoritesRepository->find($id);

        if (empty($orderFavorites)) {
            Flash::error('Order Favorites not found');

            return redirect(route('orderFavorites.index'));
        }

        $orderFavorites = $this->orderFavoritesRepository->update($request->all(), $id);

        Flash::success('Order Favorites updated successfully.');

        return redirect(route('orderFavorites.index'));
    }

    /**
     * Remove the specified OrderFavorites from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $orderFavorites = $this->orderFavoritesRepository->find($id);

        if (empty($orderFavorites)) {
            Flash::error('Order Favorites not found');

            return redirect(route('orderFavorites.index'));
        }

        $this->orderFavoritesRepository->delete($id);

        Flash::success('Order Favorites deleted successfully.');

        return redirect()->back();
    }

    public function favoriteAdd($interiorId) {
        $favorite['users_id'] = Auth::id();
        $favorite['interior_id'] = $interiorId;
        OrderFavorites::updateOrCreate($favorite);
        return redirect()->back();
    }
}
