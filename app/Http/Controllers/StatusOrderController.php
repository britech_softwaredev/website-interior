<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateStatusOrderRequest;
use App\Http\Requests\UpdateStatusOrderRequest;
use App\Repositories\StatusOrderRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class StatusOrderController extends AppBaseController
{
    /** @var  StatusOrderRepository */
    private $statusOrderRepository;

    public function __construct(StatusOrderRepository $statusOrderRepo)
    {
        $this->statusOrderRepository = $statusOrderRepo;
    }

    /**
     * Display a listing of the StatusOrder.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $statusOrders = $this->statusOrderRepository->all();

        return view('status_orders.index')
            ->with('statusOrders', $statusOrders);
    }

    /**
     * Show the form for creating a new StatusOrder.
     *
     * @return Response
     */
    public function create()
    {
        return view('status_orders.create');
    }

    /**
     * Store a newly created StatusOrder in storage.
     *
     * @param CreateStatusOrderRequest $request
     *
     * @return Response
     */
    public function store(CreateStatusOrderRequest $request)
    {
        $input = $request->all();

        $statusOrder = $this->statusOrderRepository->create($input);

        Flash::success('Status Order saved successfully.');

        return redirect(route('statusOrders.index'));
    }

    /**
     * Display the specified StatusOrder.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $statusOrder = $this->statusOrderRepository->find($id);

        if (empty($statusOrder)) {
            Flash::error('Status Order not found');

            return redirect(route('statusOrders.index'));
        }

        return view('status_orders.show')->with('statusOrder', $statusOrder);
    }

    /**
     * Show the form for editing the specified StatusOrder.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $statusOrder = $this->statusOrderRepository->find($id);

        if (empty($statusOrder)) {
            Flash::error('Status Order not found');

            return redirect(route('statusOrders.index'));
        }

        return view('status_orders.edit')->with('statusOrder', $statusOrder);
    }

    /**
     * Update the specified StatusOrder in storage.
     *
     * @param int $id
     * @param UpdateStatusOrderRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStatusOrderRequest $request)
    {
        $statusOrder = $this->statusOrderRepository->find($id);

        if (empty($statusOrder)) {
            Flash::error('Status Order not found');

            return redirect(route('statusOrders.index'));
        }

        $statusOrder = $this->statusOrderRepository->update($request->all(), $id);

        Flash::success('Status Order updated successfully.');

        return redirect(route('statusOrders.index'));
    }

    /**
     * Remove the specified StatusOrder from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $statusOrder = $this->statusOrderRepository->find($id);

        if (empty($statusOrder)) {
            Flash::error('Status Order not found');

            return redirect(route('statusOrders.index'));
        }

        $this->statusOrderRepository->delete($id);

        Flash::success('Status Order deleted successfully.');

        return redirect(route('statusOrders.index'));
    }
}
