<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateInteriorGaleriRequest;
use App\Http\Requests\UpdateInteriorGaleriRequest;
use App\Models\CategoryInterior;
use App\Models\CategoryProduct;
use App\Models\InteriorHasProduct;
use App\Repositories\InteriorGaleriRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class InteriorGaleriController extends AppBaseController
{
    /** @var  InteriorGaleriRepository */
    private $interiorGaleriRepository;

    public function __construct(InteriorGaleriRepository $interiorGaleriRepo)
    {
        $this->interiorGaleriRepository = $interiorGaleriRepo;
    }

    /**
     * Display a listing of the InteriorGaleri.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $interiorGaleris = $this->interiorGaleriRepository->all();

        return view('interior_galeris.index')
            ->with('interiorGaleris', $interiorGaleris);
    }

    /**
     * Show the form for creating a new InteriorGaleri.
     *
     * @return Response
     */
    public function create()
    {
        $categoryInterior = CategoryInterior::pluck('title','id');
        return view('interior_galeris.create',compact('categoryInterior'));
    }

    /**
     * Store a newly created InteriorGaleri in storage.
     *
     * @param CreateInteriorGaleriRequest $request
     *
     * @return Response
     */
    public function store(CreateInteriorGaleriRequest $request)
    {
        $input = $request->all();

        $interiorGaleri = $this->interiorGaleriRepository->create($input);

        $interiorGaleri->addFromMediaLibraryRequest($request->media)
            ->toMediaCollection();

        Flash::success('Interior Galeri saved successfully.');

        return redirect(route('interiorGaleris.index'));
    }

    /**
     * Display the specified InteriorGaleri.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $categoriProduk = CategoryProduct::pluck('title','id');
        $interiorGaleri = $this->interiorGaleriRepository->find($id);

        if (empty($interiorGaleri)) {
            Flash::error('Interior Galeri not found');

            return redirect(route('interiorGaleris.index'));
        }

        return view('interior_galeris.show',compact('categoriProduk'))->with('interiorGaleri', $interiorGaleri);
    }

    /**
     * Show the form for editing the specified InteriorGaleri.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $interiorGaleri = $this->interiorGaleriRepository->find($id);
        $categoryInterior = CategoryInterior::pluck('title','id');

        if (empty($interiorGaleri)) {
            Flash::error('Interior Galeri not found');

            return redirect(route('interiorGaleris.index'));
        }

        return view('interior_galeris.edit',compact('categoryInterior'))->with('interiorGaleri', $interiorGaleri);
    }

    /**
     * Update the specified InteriorGaleri in storage.
     *
     * @param int $id
     * @param UpdateInteriorGaleriRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInteriorGaleriRequest $request)
    {
        $interiorGaleri = $this->interiorGaleriRepository->find($id);

        if (empty($interiorGaleri)) {
            Flash::error('Interior Galeri not found');

            return redirect(route('interiorGaleris.index'));
        }

        $interiorGaleri = $this->interiorGaleriRepository->update($request->all(), $id);

        $interiorGaleri->syncFromMediaLibraryRequest($request->media)
            ->toMediaCollection();

        Flash::success('Interior Galeri updated successfully.');

        return redirect(route('interiorGaleris.index'));
    }

    /**
     * Remove the specified InteriorGaleri from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $interiorGaleri = $this->interiorGaleriRepository->find($id);

        if (empty($interiorGaleri)) {
            Flash::error('Interior Galeri not found');

            return redirect(route('interiorGaleris.index'));
        }

        $this->interiorGaleriRepository->delete($id);

        Flash::success('Interior Galeri deleted successfully.');

        return redirect(route('interiorGaleris.index'));
    }
}
