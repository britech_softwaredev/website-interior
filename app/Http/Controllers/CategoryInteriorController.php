<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCategoryInteriorRequest;
use App\Http\Requests\UpdateCategoryInteriorRequest;
use App\Repositories\CategoryInteriorRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class CategoryInteriorController extends AppBaseController
{
    /** @var  CategoryInteriorRepository */
    private $categoryInteriorRepository;

    public function __construct(CategoryInteriorRepository $categoryInteriorRepo)
    {
        $this->categoryInteriorRepository = $categoryInteriorRepo;
    }

    /**
     * Display a listing of the CategoryInterior.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $categoryInteriors = $this->categoryInteriorRepository->all();

        return view('category_interiors.index')
            ->with('categoryInteriors', $categoryInteriors);
    }

    /**
     * Show the form for creating a new CategoryInterior.
     *
     * @return Response
     */
    public function create()
    {
        return view('category_interiors.create');
    }

    /**
     * Store a newly created CategoryInterior in storage.
     *
     * @param CreateCategoryInteriorRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoryInteriorRequest $request)
    {
        $input = $request->all();

        $categoryInterior = $this->categoryInteriorRepository->create($input);

        Flash::success('Category Interior saved successfully.');

        return redirect(route('categoryInteriors.index'));
    }

    /**
     * Display the specified CategoryInterior.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $categoryInterior = $this->categoryInteriorRepository->find($id);

        if (empty($categoryInterior)) {
            Flash::error('Category Interior not found');

            return redirect(route('categoryInteriors.index'));
        }

        return view('category_interiors.show')->with('categoryInterior', $categoryInterior);
    }

    /**
     * Show the form for editing the specified CategoryInterior.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $categoryInterior = $this->categoryInteriorRepository->find($id);

        if (empty($categoryInterior)) {
            Flash::error('Category Interior not found');

            return redirect(route('categoryInteriors.index'));
        }

        return view('category_interiors.edit')->with('categoryInterior', $categoryInterior);
    }

    /**
     * Update the specified CategoryInterior in storage.
     *
     * @param int $id
     * @param UpdateCategoryInteriorRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoryInteriorRequest $request)
    {
        $categoryInterior = $this->categoryInteriorRepository->find($id);

        if (empty($categoryInterior)) {
            Flash::error('Category Interior not found');

            return redirect(route('categoryInteriors.index'));
        }

        $categoryInterior = $this->categoryInteriorRepository->update($request->all(), $id);

        Flash::success('Category Interior updated successfully.');

        return redirect(route('categoryInteriors.index'));
    }

    /**
     * Remove the specified CategoryInterior from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $categoryInterior = $this->categoryInteriorRepository->find($id);

        if (empty($categoryInterior)) {
            Flash::error('Category Interior not found');

            return redirect(route('categoryInteriors.index'));
        }

        $this->categoryInteriorRepository->delete($id);

        Flash::success('Category Interior deleted successfully.');

        return redirect(route('categoryInteriors.index'));
    }
}
