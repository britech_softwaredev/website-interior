<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateKategoriProdukRequest;
use App\Http\Requests\UpdateKategoriProdukRequest;
use App\Repositories\KategoriProdukRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class KategoriProdukController extends AppBaseController
{
    /** @var  KategoriProdukRepository */
    private $kategoriProdukRepository;

    public function __construct(KategoriProdukRepository $kategoriProdukRepo)
    {
        $this->kategoriProdukRepository = $kategoriProdukRepo;
    }

    /**
     * Display a listing of the KategoriProduk.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $kategoriProduks = $this->kategoriProdukRepository->all();

        return view('kategori_produks.index')
            ->with('kategoriProduks', $kategoriProduks);
    }

    /**
     * Show the form for creating a new KategoriProduk.
     *
     * @return Response
     */
    public function create()
    {
        return view('kategori_produks.create');
    }

    /**
     * Store a newly created KategoriProduk in storage.
     *
     * @param CreateKategoriProdukRequest $request
     *
     * @return Response
     */
    public function store(CreateKategoriProdukRequest $request)
    {
        $input = $request->all();

        $kategoriProduk = $this->kategoriProdukRepository->create($input);

        Flash::success('Kategori Produk saved successfully.');

        return redirect(route('kategoriProduks.index'));
    }

    /**
     * Display the specified KategoriProduk.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $kategoriProduk = $this->kategoriProdukRepository->find($id);

        if (empty($kategoriProduk)) {
            Flash::error('Kategori Produk not found');

            return redirect(route('kategoriProduks.index'));
        }

        return view('kategori_produks.show')->with('kategoriProduk', $kategoriProduk);
    }

    /**
     * Show the form for editing the specified KategoriProduk.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $kategoriProduk = $this->kategoriProdukRepository->find($id);

        if (empty($kategoriProduk)) {
            Flash::error('Kategori Produk not found');

            return redirect(route('kategoriProduks.index'));
        }

        return view('kategori_produks.edit')->with('kategoriProduk', $kategoriProduk);
    }

    /**
     * Update the specified KategoriProduk in storage.
     *
     * @param int $id
     * @param UpdateKategoriProdukRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateKategoriProdukRequest $request)
    {
        $kategoriProduk = $this->kategoriProdukRepository->find($id);

        if (empty($kategoriProduk)) {
            Flash::error('Kategori Produk not found');

            return redirect(route('kategoriProduks.index'));
        }

        $kategoriProduk = $this->kategoriProdukRepository->update($request->all(), $id);

        Flash::success('Kategori Produk updated successfully.');

        return redirect(route('kategoriProduks.index'));
    }

    /**
     * Remove the specified KategoriProduk from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $kategoriProduk = $this->kategoriProdukRepository->find($id);

        if (empty($kategoriProduk)) {
            Flash::error('Kategori Produk not found');

            return redirect(route('kategoriProduks.index'));
        }

        $this->kategoriProdukRepository->delete($id);

        Flash::success('Kategori Produk deleted successfully.');

        return redirect(route('kategoriProduks.index'));
    }
}
