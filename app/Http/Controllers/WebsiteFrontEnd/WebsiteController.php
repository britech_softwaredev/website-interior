<?php

namespace App\Http\Controllers\WebsiteFrontEnd;

use App\Http\Controllers\Controller;
use App\Http\Controllers\WebsiteFrontEnd\Utility\DataStaticController;
use App\Models\Agenda;
use App\Models\AkdCategory;
use App\Models\CategoryProduct;
use App\Models\Fraksi;
use App\Models\Galeri;
use App\Models\InteriorGaleri;
use App\Models\JenisPortofolio;
use App\Models\OrderFavorites;
use App\Models\OrderProduct;
use App\Models\Page;
use App\Models\Partner;
use App\Models\PersonDewan;
use App\Models\Portofolio;
use App\Models\Post;
use App\Models\PostCategory;
use App\Models\Rekening;
use Artesaos\SEOTools\Facades\SEOTools;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Auth;

class WebsiteController extends DataStaticController
{
    public function index() {
        $title="Home";

        SEOTools::setTitle($title);
        SEOTools::setDescription('Show '.$title);
        SEOTools::opengraph()->addProperty('type', 'home');

        return view('website.beranda.home');
    }

    public function interiorGaleri() {

        $title="Interior Galeri";

        SEOTools::setTitle($title);
        SEOTools::setDescription('Show '.$title);
        SEOTools::opengraph()->addProperty('type', 'home');

        $interior = InteriorGaleri::all();

        return view('website.interior_galery.index',compact('interior'));
    }

    public function detailInterior($slug) {
        $interior = InteriorGaleri::where('slug',$slug)->first();

        $title= $interior->title;

        SEOTools::setTitle($title);
        SEOTools::setDescription('Show '.$title);
        SEOTools::opengraph()->addProperty('type', 'home');

        return view('website.interior_galery.detail',compact('interior'));
    }

    public function product() {
        $title="Product Interior";

        SEOTools::setTitle($title);
        SEOTools::setDescription('Show '.$title);
        SEOTools::opengraph()->addProperty('type', 'home');

        $product = CategoryProduct::all();

        return view('website.product_interior.index',compact('product'));
    }

    public function checkout() {
        $title="Order Product";

        SEOTools::setTitle($title);
        SEOTools::setDescription('Show '.$title);
        SEOTools::opengraph()->addProperty('type', 'home');

        $order = OrderProduct::where('users_id',Auth::id())->whereHas('status',function ($q) {
            $q->where('name','KERANJANG');
        })->first();
        return view('website.checkout_order.index',compact('order'));
    }

    public function statusOrder() {
        $title="Status Order";

        SEOTools::setTitle($title);
        SEOTools::setDescription('Show '.$title);
        SEOTools::opengraph()->addProperty('type', 'home');

        $order = OrderProduct::where('users_id',Auth::id())->whereHas('status', function ($q) {
            $q->where('name','!=','KERANJANG');
        })->get();
        $rekening = Rekening::get();
        return view('website.order_status.index',compact('order','rekening'));
    }

    public function favoriteInterior() {
        $title="Interior Favorite";

        SEOTools::setTitle($title);
        SEOTools::setDescription('Show '.$title);
        SEOTools::opengraph()->addProperty('type', 'home');

        $interior = OrderFavorites::where('interior_id','!=', null)->where('users_id',Auth::id())->get();
        return view('website.favorite.index',compact('interior'));
    }
//
//    public function listPostByCategory($slugCategory,Request $request)
//    {
//        $listPost = Cache::remember("listPost-".$slugCategory.'-'.$request['page'],300 , function () use($slugCategory) {
//            return Post::whereHas('postCategory',function ($q) use($slugCategory){
//                $q->where('slug',$slugCategory)
//                    ->orWhereHas('parent', function ($q) use($slugCategory) {
//                        $q->where('slug', $slugCategory);
//                    });
//            })->latest()->paginate(9);
//        });
//
//        $activePostCategory=PostCategory::where('slug',$slugCategory)->first();
//
//        return view('web_frontend.post.list_post',compact('listPost','activePostCategory'));
//    }
//
//    public function detailPost($slug) {
//        $postDetail = Cache::remember("detailPost-".$slug,2600000 , function () use($slug) {
//            return Post::where('slug',$slug)->first();
//        });
//
//        if (empty($postDetail)) {
//            //Flash::error('Halaman tidak ditemukan');
//            return redirect(route('beranda'));
//        }
//
//        $title=$postDetail->title;
//
//        SEOTools::setTitle($title);
//        SEOTools::setDescription('Show '.$title);
//        SEOTools::opengraph()->addProperty('type', 'article');
//
//        views($postDetail)->cooldown(5)->record();
//        return view('web_frontend.post.detail_post',compact('postDetail'));
//    }
//
//    public function detailPage($slug) {
//        $pageDetail = Cache::remember("detailPage-".$slug,2600000 , function () use($slug) {
//            return Page::where('slug',$slug)->first();
//        });
//
//        if (empty($pageDetail)) {
//            //Flash::error('Halaman tidak ditemukan');
//            return redirect(route('beranda'));
//        }
//
//        $title=$pageDetail->title;
//
//        SEOTools::setTitle($title);
//        SEOTools::setDescription('Show '.$title);
//        SEOTools::opengraph()->addProperty('type', 'article');
//
//        views($pageDetail)->cooldown(5)->record();
//        return view('web_frontend.detail_page.detail_page',compact('pageDetail'));
//    }
//
//    public function beritaSearch(Request $request) {
//        $key = $request->pencarian;
//        $berita = Post::whereHas('postCategory', function ($query) use ($request) {
//            $query->where('name','=',$request->pencarian);
//        })->orWhere('title','like','%'.$request->pencarian.'%')->paginate(8);
//
//        $listPost = Post::paginate(8);
//
//        $title="Search Article";
//
//        SEOTools::setTitle($title);
//        SEOTools::setDescription('Show '.$title);
//        SEOTools::opengraph()->addProperty('type', 'article');
//
//        return view('web_frontend.post.result_post',compact('berita','key','listPost'));
//    }

//    public function galeriFoto() {
//        $title = 'Galeri Foto DPRD';
//        $listGaleri = Galeri::latest()->paginate(20);
//
//        SEOTools::setTitle($title);
//        SEOTools::setDescription('Show '.$title);
//        SEOTools::opengraph()->addProperty('type', 'image');
//
//        return view('web_frontend.galeri_foto.galeri_foto',compact('listGaleri'));
//    }
//
//    public function detailGaleriFoto($slug) {
//
//        $galeri = Galeri::where("slug",$slug)->first();
//
//        if (empty($galeri)) {
//            return redirect(route('beranda'));
//        }
//
//        SEOTools::setTitle($galeri->title);
//        SEOTools::setDescription('Show '.$galeri->title);
//        SEOTools::opengraph()->addProperty('type', 'image');
//
//        return view('web_frontend.galeri_foto.detail_galeri_foto',compact('galeri'));
//    }

//    public function detailInstagram($slug){
//        $title = 'Kabar Instagram';
//        try{
//            $instagram = new \InstagramScraper\Instagram(new \GuzzleHttp\Client());
//            $instagramMedias = $instagram->getMedias('dprdkaltimofficial');
//        }catch (\Exception $e){
//            $instagramMedias=null;
//        }
//        return view('web_frontend.beranda.image_instagram',compact('slug','instagramMedias','title'));
//    }

//    public function agendaDprd() {
//        $agendaDprd = Agenda::orderBy('schedule_date','desc')->paginate(20);
//        $title="Agenda DPRD";
//        $listPost = Post::paginate(8);
//        SEOTools::setTitle($title);
//        SEOTools::setDescription('Show '.$title);
//        SEOTools::opengraph()->addProperty('type', 'home');
//        return view('web_frontend.agenda.agenda_dprd',compact('agendaDprd','listPost'));
//    }
//
//    public function detailAgenda($slug) {
//        $agendaDetail = Cache::remember("detailAgenda-".$slug,2600000 , function () use($slug) {
//            return Agenda::where('slug',$slug)->first();
//        });
//
//        return view('web_frontend.agenda.detail_agenda',compact('agendaDetail'));
//    }
//
//    public function portofolio(Request $request) {
//        $listPortofolio=Portofolio::paginate(10);
//        $jenisPortofolio = JenisPortofolio::all();
//
//        return view('website.portofolio.portofolio',compact('listPortofolio','jenisPortofolio'));
//    }
}
