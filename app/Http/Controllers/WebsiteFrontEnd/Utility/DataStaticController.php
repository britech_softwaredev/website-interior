<?php


namespace App\Http\Controllers\WebsiteFrontEnd\Utility;


use App\Http\Controllers\Controller;
use App\Models\OrderProduct;
use Auth;
use Illuminate\Support\Facades\View;

class DataStaticController extends Controller
{
    public function __construct()
    {
        $order = OrderProduct::where('users_id',Auth::id())->first();
        return View::share(compact('order'));
    }
}
