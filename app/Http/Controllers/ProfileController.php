<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProfileRequest;
use App\Http\Requests\UpdateProfileRequest;
use App\Models\Profile;
use App\Repositories\ProfileRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ProfileController extends AppBaseController
{
    /** @var  ProfileRepository */
    private $profileRepository;

    public function __construct(ProfileRepository $profileRepo)
    {
        $this->profileRepository = $profileRepo;
    }

    /**
     * Display a listing of the Profile.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $this->middleware(['auth','permissions:profiles.index']);
        $profiles = $this->profileRepository->all();

        return view('profiles.index')
            ->with('profiles', $profiles);
    }

    /**
     * Show the form for creating a new Profile.
     *
     * @return Response
     */
    public function create()
    {
        $this->middleware(['auth','permissions:profiles.create']);
        return view('profiles.create');
    }

    /**
     * Store a newly created Profile in storage.
     *
     * @param CreateProfileRequest $request
     *
     * @return Response
     */
    public function store(CreateProfileRequest $request)
    {
        $this->middleware(['auth','permissions:profiles.store']);
        $input = $request->except('value');

        if (isset($input['value_text'])) {
            $input['value'] = $input['value_text'];
        } else if (isset($input['value_file'])) {
            if ($request->hasFile('value_file')) {
                $file = $request->file('value_file');
                $filename = $file->getClientOriginalName();
                $path = $request['value_file']->storeAs('public/profil', $filename, 'local');
                $input['value'] = 'storage' . substr($path, strpos($path, '/'));
            }
        }

        $profile = $this->profileRepository->create($input);

        Flash::success('Profile saved successfully.');

        return redirect(route('profiles.index'));
    }

    /**
     * Display the specified Profile.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $profile = $this->profileRepository->find($id);

        if (empty($profile)) {
            Flash::error('Profile not found');

            return redirect(route('profiles.index'));
        }

        return view('profiles.show')->with('profile', $profile);
    }

    /**
     * Show the form for editing the specified Profile.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->middleware(['auth','permissions:profiles.edit']);
        $profile = $this->profileRepository->find($id);

        if (empty($profile)) {
            Flash::error('Profile not found');

            return redirect(route('profiles.index'));
        }

        return view('profiles.edit')->with('profile', $profile);
    }

    /**
     * Update the specified Profile in storage.
     *
     * @param int $id
     * @param UpdateProfileRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProfileRequest $request)
    {
        $this->middleware(['auth','permissions:profiles.update']);
        $profile = $this->profileRepository->find($id);

        $input = $request->except('value');

        if (isset($input['value_text'])) {
            $input['value'] = $input['value_text'];
        } else if (isset($input['value_file'])) {
            if ($request->hasFile('value_file')) {
                $file = $request->file('value_file');
                $filename = $file->getClientOriginalName();
                $path = $request['value_file']->storeAs('public/profil', $filename, 'local');
                $input['value'] = 'storage' . substr($path, strpos($path, '/'));
            }
        }

        if (empty($profile)) {
            Flash::error('Profile not found');

            return redirect(route('profiles.index'));
        }

        $profile = $this->profileRepository->update($input, $id);

        Flash::success('Profile updated successfully.');

        return redirect(route('profiles.index'));
    }

    /**
     * Remove the specified Profile from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->middleware(['auth','permissions:profiles.destroy']);
        $profile = $this->profileRepository->find($id);

        if (empty($profile)) {
            Flash::error('Profile not found');

            return redirect(route('profiles.index'));
        }

        $this->profileRepository->delete($id);

        Flash::success('Profile deleted successfully.');

        return redirect(route('profiles.index'));
    }

    public function editInfo($id) {
        $profil = Profile::find($id);
        $profil['value'] = '';
        $profil->save();
        return redirect(route('profiles.index'));
    }
}
