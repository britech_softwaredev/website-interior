<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateGaleriRequest;
use App\Http\Requests\UpdateGaleriRequest;
use App\Repositories\GaleriRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Cache;
use Response;
use Auth;

class GaleriController extends AppBaseController
{
    /** @var  GaleriRepository */
    private $galeriRepository;

    public function __construct(GaleriRepository $galeriRepo)
    {
        $this->galeriRepository = $galeriRepo;
    }

    /**
     * Display a listing of the Galeri.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $this->middleware(['auth','permissions:galeris.index']);
        $galeris = $this->galeriRepository->all();

        return view('galeris.index')
            ->with('galeris', $galeris);
    }

    /**
     * Show the form for creating a new Galeri.
     *
     * @return Response
     */
    public function create()
    {
        $this->middleware(['auth','permissions:galeris.create']);        
        return view('galeris.create');
    }

    /**
     * Store a newly created Galeri in storage.
     *
     * @param CreateGaleriRequest $request
     *
     * @return Response
     */
    public function store(CreateGaleriRequest $request)
    {
        $this->middleware(['auth','permissions:galeris.store']);
        $input = $request->all();
        $input['users_created_id'] = Auth::user()->id;
        $input['users_updated_id'] = Auth::user()->id;

        $galeri = $this->galeriRepository->create($input);

        $galeri->addFromMediaLibraryRequest($request->media)
            ->toMediaCollection();

        Cache::forget('home_galeri');

        Flash::success('Galeri saved successfully.');

        return redirect(route('galeris.index'));
    }

    /**
     * Display the specified Galeri.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $galeri = $this->galeriRepository->find($id);

        if (empty($galeri)) {
            Flash::error('Galeri not found');

            return redirect(route('galeris.index'));
        }

        return view('galeris.show')->with('galeri', $galeri);
    }

    /**
     * Show the form for editing the specified Galeri.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->middleware(['auth','permissions:galeris.edit']);
        $galeri = $this->galeriRepository->find($id);

        if (empty($galeri)) {
            Flash::error('Galeri not found');

            return redirect(route('galeris.index'));
        }

        return view('galeris.edit')->with('galeri', $galeri);
    }

    /**
     * Update the specified Galeri in storage.
     *
     * @param int $id
     * @param UpdateGaleriRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGaleriRequest $request)
    {
        $this->middleware(['auth','permissions:galeris.update']);
        $galeri = $this->galeriRepository->find($id);

        if (empty($galeri)) {
            Flash::error('Galeri not found');

            return redirect(route('galeris.index'));
        }

        $galeri = $this->galeriRepository->update($request->all(), $id);

        $galeri->syncFromMediaLibraryRequest($request->media)
            ->toMediaCollection();
        Cache::forget('home_galeri');
        Flash::success('Galeri updated successfully.');

        return redirect(route('galeris.index'));
    }

    /**
     * Remove the specified Galeri from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->middleware(['auth','permissions:galeris.destory']);
        $galeri = $this->galeriRepository->find($id);

        if (empty($galeri)) {
            Flash::error('Galeri not found');

            return redirect(route('galeris.index'));
        }

        $this->galeriRepository->delete($id);
        Cache::forget('home_galeri');
        Flash::success('Galeri deleted successfully.');

        return redirect(route('galeris.index'));
    }
}
