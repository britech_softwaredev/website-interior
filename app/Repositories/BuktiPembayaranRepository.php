<?php

namespace App\Repositories;

use App\Models\BuktiPembayaran;
use App\Repositories\BaseRepository;

/**
 * Class BuktiPembayaranRepository
 * @package App\Repositories
 * @version August 15, 2022, 12:12 pm UTC
*/

class BuktiPembayaranRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'deskripsi',
        'order_product_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BuktiPembayaran::class;
    }
}
