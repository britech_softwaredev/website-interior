<?php

namespace App\Repositories;

use App\Models\Rekening;
use App\Repositories\BaseRepository;

/**
 * Class RekeningRepository
 * @package App\Repositories
 * @version August 15, 2022, 12:06 pm UTC
*/

class RekeningRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'bank',
        'deskripsi'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Rekening::class;
    }
}
