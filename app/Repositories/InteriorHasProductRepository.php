<?php

namespace App\Repositories;

use App\Models\InteriorHasProduct;
use App\Repositories\BaseRepository;

/**
 * Class InteriorHasProductRepository
 * @package App\Repositories
 * @version June 13, 2022, 11:16 am UTC
*/

class InteriorHasProductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return InteriorHasProduct::class;
    }
}
