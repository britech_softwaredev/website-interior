<?php

namespace App\Repositories;

use App\Models\Produk;
use App\Repositories\BaseRepository;

/**
 * Class ProdukRepository
 * @package App\Repositories
 * @version June 12, 2022, 11:53 pm UTC
*/

class ProdukRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'title_eng',
        'description',
        'description_eng',
        'price',
        'color',
        'material',
        'dimensions',
        'produsen',
        'slug',
        'category_product_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Produk::class;
    }
}
