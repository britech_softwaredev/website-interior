<?php

namespace App\Repositories;

use App\Models\CategoryProduct;
use App\Repositories\BaseRepository;

/**
 * Class KategoriProdukRepository
 * @package App\Repositories
 * @version June 12, 2022, 11:52 pm UTC
*/

class KategoriProdukRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'title_eng',
        'description',
        'description_eng',
        'slug'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CategoryProduct::class;
    }
}
