<?php

namespace App\Repositories;

use App\Models\CategoryInterior;
use App\Repositories\BaseRepository;

/**
 * Class CategoryInteriorRepository
 * @package App\Repositories
 * @version June 11, 2022, 3:14 am UTC
*/

class CategoryInteriorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'title_eng',
        'description',
        'description_eng',
        'slug'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CategoryInterior::class;
    }
}
