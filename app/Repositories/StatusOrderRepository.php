<?php

namespace App\Repositories;

use App\Models\StatusOrder;
use App\Repositories\BaseRepository;

/**
 * Class StatusOrderRepository
 * @package App\Repositories
 * @version June 14, 2022, 1:31 am UTC
*/

class StatusOrderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StatusOrder::class;
    }
}
