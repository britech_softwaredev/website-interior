<?php

namespace App\Repositories;

use App\Models\InteriorGaleri;
use App\Repositories\BaseRepository;

/**
 * Class InteriorGaleriRepository
 * @package App\Repositories
 * @version June 11, 2022, 3:10 am UTC
*/

class InteriorGaleriRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'title_eng',
        'description',
        'description_eng',
        'price',
        'dimensions',
        'slug',
        'category_interior_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return InteriorGaleri::class;
    }
}
