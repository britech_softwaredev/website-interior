<?php

namespace App\Repositories;

use App\Models\OrderFavorites;
use App\Repositories\BaseRepository;

/**
 * Class OrderFavoritesRepository
 * @package App\Repositories
 * @version June 14, 2022, 1:38 am UTC
*/

class OrderFavoritesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'users_id',
        'uuid',
        'product_id',
        'interior_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrderFavorites::class;
    }
}
