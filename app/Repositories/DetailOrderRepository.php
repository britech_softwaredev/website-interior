<?php

namespace App\Repositories;

use App\Models\DetailOrder;
use App\Repositories\BaseRepository;

/**
 * Class DetailOrderRepository
 * @package App\Repositories
 * @version June 14, 2022, 4:09 am UTC
*/

class DetailOrderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'order_product_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DetailOrder::class;
    }
}
