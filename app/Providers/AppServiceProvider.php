<?php

namespace App\Providers;

use App\Models\AkdCategory;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Carbon::setLocale('id');
        setlocale(LC_TIME, 'id_ID');

        //$akdCategoriesMenu=AkdCategory::whereNull('parent_id')->orderBy('order')->withCount('childs')->get();

        //view()->share('akdCategoriesMenu', $akdCategoriesMenu);
    }
}
