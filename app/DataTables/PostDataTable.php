<?php

namespace App\DataTables;

use App\Models\Post;
use Carbon\Carbon;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class PostDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'posts.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Post $model)
    {
        return $model->newQuery()->latest()->with(['post_category','users_updated','users_created']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->serverSide(true)
            ->processing(true)
            ->paging(true)
            ->lengthMenu([10,25,50,100])
            ->pageLength(10)
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => ['title' => 'No.', 'orderable' => false, 'searchable' => false, 'render' => function() {
                return 'function(data,type,fullData,meta){return meta.settings._iDisplayStart+meta.row+1;}';
            }],
            'title'=>new Column(['title'=>'Judul','data'=>'title','name'=>'title']),
            'image_caption'=>new Column(['title'=>'Judul Foto','data'=>'image_caption','name'=>'image_caption']),
            'post_category_id'=>new Column([
                'title' => 'Kategori',
                'data' => 'post_category.name',
                'name' => 'post_category.name',
                'defaultContent' => '-'
            ]),
            'users_created_id'=>new Column([
                'title'=>'Pembuat',
                'data' => 'users_created.name',
                'name' => 'users_created.name',
                'defaultContent' => '-']),
            'users_updated_id'=>new Column([
                'title'=>'Pengubah',
                'data' => 'users_updated.name',
                'name' => 'users_updated.name',
                'defaultContent' => '-']),
            'slug',
            'created_at_standard'=>new Column([
                'title'=>'Tanggal',
                'data' => 'created_at_standard',
                'name' => 'created_at_standard',
                'defaultContent' => '-']),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'postsdatatable_' . time();
    }
}
