<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Product Id:') !!}
    <div class="position-relative">
        {!! Form::number('product_id', null, ['class' => 'form-control']) !!}
    </div>
</div>


<!-- Order Product Id Field -->
<div class="form-group">
    {!! Form::label('order_product_id', 'Order Product Id:') !!}
    <div class="position-relative">
        {!! Form::number('order_product_id', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Submit Field -->
<div class="form-actions center">
    <a href="{!! route('detailOrders.index') !!}" class="btn btn-danger"> <i class="fa fa-close"></i> Batal</a>
    {!! Form::submit('Simpan', ['class' => 'btn btn-green mr-1']) !!}
</div>

