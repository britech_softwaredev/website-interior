<!-- Users Id Field -->
<div class="form-group">
    {!! Form::label('users_id', 'Users Id:') !!}
    <div class="position-relative">
        {!! Form::number('users_id', null, ['class' => 'form-control']) !!}
    </div>
</div>


<!-- Uuid Field -->
<div class="form-group">
    {!! Form::label('uuid', 'Uuid:') !!}
    <div class="position-relative">
        {!! Form::text('uuid', null, ['class' => 'form-control']) !!}
    </div>
</div>


<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Product Id:') !!}
    <div class="position-relative">
        {!! Form::number('product_id', null, ['class' => 'form-control']) !!}
    </div>
</div>


<!-- Interior Id Field -->
<div class="form-group">
    {!! Form::label('interior_id', 'Interior Id:') !!}
    <div class="position-relative">
        {!! Form::number('interior_id', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Submit Field -->
<div class="form-actions center">
    <a href="{!! route('orderFavorites.index') !!}" class="btn btn-danger"> <i class="fa fa-close"></i> Batal</a>
    {!! Form::submit('Simpan', ['class' => 'btn btn-green mr-1']) !!}
</div>

