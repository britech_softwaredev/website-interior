<!-- Sudah di modifikasi untuk Edit,Lihat,Hapus -->
<table class="table table-hover table-bordered table-striped default table-responsive">
    <colgroup>
        <col class="col-xs-1">
        <col class="col-xs-7">
    </colgroup>
    <thead>
    <tr>
        <th><code>#</code></th>
        <th>Users Id</th>
        <th>Uuid</th>
        <th>Product Id</th>
        <th>Interior Id</th>
        <th style="text-align: center">Action</th>
    </tr>
    </thead>
    <tbody>
    @php
        $no = 1;
    @endphp
    @foreach($orderFavorites as $orderFavorites)
        <tr>
            <td>{!! $no++ !!}</td>
            <td>{!! $orderFavorites->users_id !!}</td>
            <td>{!! $orderFavorites->uuid !!}</td>
            <td>{!! $orderFavorites->product_id !!}</td>
            <td>{!! $orderFavorites->interior_id !!}</td>
            <td>
                {!! Form::open(['route' => ['orderFavorites.destroy', $orderFavorites->id], 'method' => 'delete']) !!}
                <div class="btn-group" role="group" aria-label="Basic example">
                    <a href="{!! route('orderFavorites.show', [$orderFavorites->id]) !!}" class="btn btn-sm btn-success"><i class="fa fa-eye"></i></a>
                                       <a href="{!! route('orderFavorites.edit', [$orderFavorites->id]) !!}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
                                       {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-sm btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
