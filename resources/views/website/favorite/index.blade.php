@extends('website.layout.app')
@section('content')
<section class="page-section white-bg  page-section-ptb pb-10">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="card-body pt-0 pl-0">
                    <h1 class="text-black">Interior Favorite</h1>
                    <div>Semua design interior yang anda suka tersimpan</div>
                </div>
                <div class="divider dashed mt-30"></div>

            </div>
        </div>
    </div>
</section>
<section class="shop-06-product mt-50 mb-50">
    <div class="container">
        <div class="row">
            @foreach($interior as $item)
                <div class="col-md-4 col-sm-3 xs-mb-30 mb-20">
                    <div class="blog blog-simple text-start rounded">
                        <div class="blog-image">
                            <img class="img-fluid" src="{{ url($item['interior']->getFirstMediaUrl()) }}" alt="">
                        </div>
                        <div class="blog-name mt-20">
                            <h6 class="text-brown mt-15">{{ $item['interior']['categoryInterior']['title'] }}</h6>
                            <h5><a href="{{ ('interior/'.$item['interior']['slug']) }}">{{ $item['interior']['title'] }}</a></h5>
                            <div class="desc-p">{{ $item['interior']['description'] }}</div>
                            {!! Form::open(['route' => ['orderFavorites.destroy', $item->id], 'method' => 'delete']) !!}
                            {!! Form::button('<i class="fa fa-trash font-small-2"></i>', ['type' => 'submit', 'class' => 'btn btn-sm btn-danger mt-10']) !!}
                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            @endforeach
            <div class="col-12">
            </div>
        </div>
    </div>
</section>

@endsection
