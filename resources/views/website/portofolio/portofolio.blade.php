@extends('website.layout.app')
@section('content')

    <div class="container medium-padding120">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="heading align-center">
                    <div class="h2 heading-title text-bold-800 mb-0">Portofolio</div>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>
                    <p class="heading-text">Beberapa Portofolio Aplikasi maupun Kegiatan yang dihasilkan oleh CV. Britech
                    </p>
                </div>
            </div>
        </div>

{{--        <div class="row">--}}
{{--            <div class="case-item-wrap">--}}
{{--                @foreach($listPortofolio as $item)--}}
{{--                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">--}}
{{--                        <div class="case-item">--}}
{{--                            <div class="case-item__thumb mb-0">--}}
{{--                                <article class="hentry post post-standard clear">--}}
{{--                                    <div class="post-thumb clear">--}}
{{--                                        <img src="{{ $item->getFirstMediaUrl('default') }}" alt="seo">--}}
{{--                                        <div class="overlay"></div>--}}
{{--                                        <a href="{{ $item->getFirstMediaUrl('default') }}" class="link-image js-zoom-image">--}}
{{--                                            <i class="seoicon-zoom"></i>--}}
{{--                                        </a>--}}
{{--                                        <a href="{{ isset($item->url)?$item->url:"#" }}" class="link-post" target="_blank">--}}
{{--                                            <i class="seoicon-link-bold"></i>--}}
{{--                                        </a>--}}
{{--                                    </div>--}}
{{--                                </article>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                @endforeach--}}
{{--            </div>--}}
{{--        </div>--}}

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="clients-grid">

                <ul class="cat-list-bg-style align-center sorting-menu">
                    <li class="cat-list__item active" data-filter="*"><a href="#" class="">Semua</a></li>
                    @foreach($jenisPortofolio as $item)
                        <li class="cat-list__item" data-filter=".{!! $item->name !!}"><a href="#" class="">{!! $item->name !!}</a></li>
                    @endforeach
                </ul>

                <div class="row sorting-container" id="clients-grid" data-layout="masonry">
                    @foreach($listPortofolio as $item)
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 sorting-item {{ $item['jenisPortofolio']['name'] }} smm">
                            <div class="client-item-style2 col-3 bg-border-color mb30 p-0">
                                <div class="client-image mb-1">
                                    <img src="{{ $item->getFirstMediaUrl('default') }}" alt="logo">
                                </div>
                                <div class="p-2 pt-0" style="height: 100px">
                                    <p class="clients-item-text pb-0 mb-0">{!! $item['jenisPortofolio']['name'] !!}</p>
                                    <a href="{{ $item->link }}" class="font-small-4 text-bold-800 c-dark pb-1">{!! $item->name !!}</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

            </div>
        </div>

    </div>

@endsection
