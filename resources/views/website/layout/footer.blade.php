{{--<footer class="footer  page-section-pt">--}}
{{-- <div class="container">--}}
{{--  <div class="row text-center">--}}
{{--   <div class="col-lg-4 col-md-4">--}}
{{--      <div class="contact-add mb-30">--}}
{{--       <div class="text-center">--}}
{{--           <i class="ti-map-alt text-white"></i>--}}
{{--           <h5 class="mt-15">Address</h5>--}}
{{--           <p>17504 Carlton Cuevas Rd, Gulfport, MS, 39503</p>--}}
{{--          </div>--}}
{{--      </div>--}}
{{--   </div>--}}
{{--   <div class="col-lg-4 col-md-4">--}}
{{--      <div class="contact-add mb-30">--}}
{{--       <div class="text-center">--}}
{{--         <i class="ti-mobile text-white"></i>--}}
{{--         <h5 class="mt-15">Call Us</h5>--}}
{{--         <p> +(704) 279-1249</p>--}}
{{--        </div>--}}
{{--      </div>--}}
{{--   </div>--}}
{{--   <div class="col-lg-4 col-md-4">--}}
{{--      <div class="contact-add mb-30">--}}
{{--        <div class="text-center">--}}
{{--         <i class="ti-email text-white"></i>--}}
{{--         <h5 class="mt-15">Email Us</h5>--}}
{{--         <p>letstalk@webster.com</p>--}}
{{--       </div>--}}
{{--      </div>--}}
{{--   </div>--}}
{{--   </div>--}}
{{--    <div class="row justify-content-center">--}}
{{--    <div class="col-lg-4 col-md-6">--}}
{{--       <div class="footer-Newsletter text-center mt-30 mb-40">--}}
{{--        <div id="mc_embed_signup_scroll">--}}
{{--          <form action="php/mailchimp-action.php" method="POST" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate">--}}
{{--           <div id="msg"> </div>--}}
{{--            <div id="mc_embed_signup_scroll_2">--}}
{{--              <input id="mce-EMAIL" class="form-control" type="text" placeholder="Email address" name="email1" value="">--}}
{{--            </div>--}}
{{--            <div id="mce-responses" class="clear">--}}
{{--              <div class="response" id="mce-error-response" style="display:none"></div>--}}
{{--              <div class="response" id="mce-success-response" style="display:none"></div>--}}
{{--              </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->--}}
{{--              <div style="position: absolute; left: -5000px;" aria-hidden="true">--}}
{{--                  <input type="text" name="b_b7ef45306f8b17781aa5ae58a_6b09f39a55" tabindex="-1" value="">--}}
{{--              </div>--}}
{{--              <div class="clear">--}}
{{--                <button type="submit" name="submitbtn" id="mc-embedded-subscribe" class="button button-border mt-20 form-button">  Get notified </button>--}}
{{--              </div>--}}
{{--            </form>--}}
{{--          </div>--}}
{{--        </div>--}}
{{--     </div>--}}
{{--    </div>--}}
{{--   <div class="footer-widget mt-20">--}}
{{--    <div class="row">--}}
{{--      <div class="col-lg-6 col-md-6">--}}
{{--       <p class="mt-15"> &copy;Copyright <span id="copyright"> <script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script></span> <a href="#"> Webster </a> All Rights Reserved </p>--}}
{{--      </div>--}}
{{--      <div class="col-lg-6 col-md-6">--}}
{{--        <div class="social-icons color-hover float-right">--}}
{{--         <ul>--}}
{{--          <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>--}}
{{--          <li class="social-instagram"><a href="#"><i class="fa fa-instagram"></i></a></li>--}}
{{--          <li class="social-dribbble"><a href="#"><i class="fa fa-dribbble"></i> </a></li>--}}
{{--          <li class="social-linkedin"><a href="#"><i class="fa fa-linkedin"></i> </a></li>--}}
{{--         </ul>--}}
{{--       </div>--}}
{{--      </div>--}}
{{--    </div>--}}
{{--  </div>--}}
{{--  </div>--}}
{{--</footer>--}}
<footer class="footer page-section-pt">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-5 sm-mt-30">
                <div class="about-content">
                    <a href="index-01.html"><img id="logo_img" class="height-70 mb-20" src="{{ asset('logo/royan_logo.png') }}" alt="logo"> </a>
                    <p>Jika Anda tidak puas dengan desain kami, kami tidak akan senang. Kami bekerja tanpa lelah untuk memastikan pengalaman desain yang Anda suka dan membuat anda nyaman juga menyenangkan. Itu sebabnya kami menawarkan Jaminan Kebahagiaan Rinaroyan</p>
                    <div class="social text-left sm-mt-0 xs-mt-20">
                        <ul>
                            <li>
                                <a href="royan.interior"> <i class="fa fa-facebook"></i> </a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/royan.interior/"> <i class="fa fa-instagram"></i> </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-7 col-md-7 sm-mt-30 sm-mb-30">
                <div class="usefull-link">
                    <div class="row">
                        <div class="col-lg-4 col-sm-4">
                            <ul>
                                <li><a href="#">Interior Galeri</a></li>
                                <li><a href="#">Produk Interior</a></li>
                                <li><a href="#">Informasi</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-8 col-sm-4">
                            <ul>
                                <li><i class="fa fa-map-marker"></i><span> Workshop: Jl. Jakarta Gg. Tugu Monas X Loa Bakung Samarinda</span> </li>
                                <li><i class="fa fa-map-marker"></i><span> Galeri: Perum. Karpotek Blok W No.10</span> </li>
                                <li><i class="fa fa-whatsapp"> </i> <a href="tel:0811580230"> <span>0811580230 </span> </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="copyright gray-bg mt-50">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-left text-md-center xs-mt-15">
                        <p class="mb-0"> &copy;HakCipta <span id="copyright"> <script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script></span> - <a href="#" class="text-bold-600"> Royan Interior Design </a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
