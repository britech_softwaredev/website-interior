<header id="header" class="header light">

    <!--=================================
     mega menu -->

    <div class="menu">
        <!-- menu start -->
        <nav id="menu" class="mega-menu">
            <!-- menu list items container -->
            <section class="menu-list-items">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <!-- menu logo -->
                            <ul class="menu-logo">
                                <li>
                                    <a href="{{ url('/') }}"><img id="logo_img" src="{{ asset('logo/royan_logo.png') }}" alt="logo"> </a>
                                </li>
                            </ul>
                            <!-- menu links -->
                            <div class="menu-bar">
                                <ul class="menu-links">
                                    <li class="{{ Request::is('/') ? 'active' : '' }}">
                                        <a href="{{ url('/')}}">Beranda</a>
                                    </li>
                                    <li class="{{ Request::is('interior*') ? 'active' : ''}}">
                                        <a href="{{ route('interior-galeri')}}">Interior Galeri</a>
                                    </li>
                                    <li class="{{ Request::is('product*') ? 'active' : ''}}">
                                        <a href="{{ route('product-interior')}}">Produk Interior</a>
                                    </li>
{{--                                    <li>--}}
{{--                                        <a href="javascript:void(0)">Informasi</a>--}}
{{--                                    </li>--}}
                                    <li class="{{ Request::is('orderStatus') ? 'active' : ''}}">
                                        <a href="{{ route('order_status')}}">Order</a>
                                    </li>
                                    <li>
                                        <div class="shpping-cart">
                                            <a class="cart-btn" href="{{ url('favorite') }}"> <i class="fa fa-heart icon text-black"></i>
                                                <strong class="item">
                                                    {{ \App\Models\OrderFavorites::where('users_id',Auth::id())->count() }}
                                                </strong>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="shpping-cart ml-10">
                                            <a class="cart-btn" href="{{ url('checkout') }}"> <i class="fa fa-shopping-basket icon text-black"></i>
                                                <strong class="item">
                                                    {{ \App\Models\DetailOrder::with(['orderProduct','orderProduct','orderProduct.status'])->get()->where('orderProduct.users_id',Auth::id())->where('orderProduct.status.name','KERANJANG')->count() }}
                                                </strong>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        @if(Auth::check())
                                                <a href="{!! url('/logout') !!}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="ml-10">{{ Auth::user()->display_name }} <span class="text-brown">Logout</span></a>
                                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                        @else
                                            <a href="{!! url('/login') !!}" class="ml-10"><span class="text-brown">Login</span></a>
                                        @endif
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </nav>
        <!-- menu end -->
    </div>
</header>
