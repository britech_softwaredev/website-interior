@extends('website.layout.app')
@section('content')
    <section class="page-section white-bg  page-section-ptb pb-10">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-body pt-0 pl-0">
                        <h1 class="text-black">Status Order</h1>
                        <div>Semua pesanan anda akan terlihat pada halaman ini.</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="wishlist-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Kode Order</th>
                                <th>Date Order</th>
                                <th>Order Status</th>
                                <th>Furniture Order</th>
                                <th>Price Total</th>
                                <th>Chat Admin</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $no =1;
                            @endphp
                            @if(!empty($order))
                                @foreach($order as $item)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>
                                            <span class="text-red text-bold-700">ROYAN-{{ $item->id }}</span>
                                        </td>
                                        <td>
                                            <b>{{ \Carbon\Carbon::parse($item->created_at)->format('d F Y') }}</b>
                                        </td>
                                        <td>
                                            <span class="{{ $item['status']['description'] }} font-small-1 text-white">{{ $item['status']['name'] }}</span>
                                            <div><a href="" data-toggle="modal" data-target="#modal-{{ $item->id }}"><i class="ti-cloud-up"></i> Bukti Transaksi</a></div>
                                            @include('website.modal.konfirm_pembayaran')
                                        </td>
                                        <td>
                                            @php
                                                $price=0;
                                            @endphp
                                            @foreach($item['detailOrders'] as $items)
                                                <div class="mt-10">
                                                    <div class="d-flex">
                                                        <img class="img-fluid width-100" src="{{ url($items['product']->getFirstMediaUrl()) }}" alt=""/>
                                                        <div class="pl-10">
                                                            <span class="text-bold-600">{{ $items['product']['title'] }}</span><br>
                                                            Rp. {{ number_format( $items['product']['price'],0,',','.')  }}
                                                        </div>
                                                    </div>
                                                </div>
                                                @php
                                                    $price += $items['product']['price']
                                                @endphp
                                            @endforeach
                                        </td>
                                        <td>Rp. {{ number_format($price,0,',','.')  }}</td>
                                        <td class="text-center">
                                            <a href="https://wa.me/6283140141152" target="_blank"><i class="fa fa-whatsapp theme-color font-medium-1"></i></a>
                                        </td>
                                    </tr>
                                    @if(!empty($item->description_status))
                                        <tr>
                                            <td class="border-0"></td>
                                            <td class="border-0"></td>
                                            <td class="border-0"></td>
                                            <td class="border-0" colspan="2">
                                                <div class="divider mb-10 mt-10"></div>
                                                <b>Catatan Status Order : </b><br> {{ $item->description_status }}
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="divider dashed mt-30"></div>

                </div>
            </div>
        </div>
    </section>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/media-pro/styles.css')}}">

    <livewire:styles />
    <livewire:scripts />

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.6.0/dist/alpine.min.js" defer></script>
@endsection
