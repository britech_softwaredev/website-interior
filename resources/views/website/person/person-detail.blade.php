@extends('website.layout.app')
@section('content')
{{--    <div class="stunning-header stunning-header-bg-olive" style="padding: 54px 0"></div>--}}

    <div class="container medium-padding120 pt200">
        <div class="row product-details">
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                <div class="product-details-thumb">
                    <div class="swiper-container" data-effect="fade">
                        <!-- Additional required wrapper -->
                        <div class="swiper-wrapper">
                            <!-- Slides -->
                            <div class="product-details-img-wrap swiper-slide">
                                <img src="{{ $person->getFirstMediaUrl('default') }}" alt="product" data-swiper-parallax="-10">
                            </div>
                        </div>
                        <!-- If we need pagination -->
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-lg-offset-1 col-md-6 col-md-offset-1 col-sm-6 col-sm-offset-1 col-xs-12 col-xs-offset-0">
                <div class="product-details-info pb-0">
                    <h2 class="product-details-info-title text-bold-800">{{ $person->name }}</h2>
                    <div class="mb-0 text-bold-600 font-medium-3 c-gray-dark">{{ $person['jabatan']['name'] }}</div>
                    <div class="product-details-info-ratings">
                        <div class="reviews c-secondary"><i class="fa fa-calendar"></i> Bergabung : {{ \Carbon\Carbon::parse($person->join_company)->format('d F Y') }}</div>
                    </div>
                    <p class="product-details-info-text">{!! $person->description !!}</p>
                </div>

                <div class="product-details-add-info">
                    <div class="author">Potofolio:
                        <a href="#" class="author-name c-green">{{ count($person['personPorto']) }} Projek</a>
                    </div>
                    <div class="tags">Penguasaan:
                        @foreach($person['personSkill'] as $item)
                            <a class="tags-item">{{ $item['skill']->name }} ,</a>
                        @endforeach
                    </div>

                    <div class="socials">Sosial Media:
                        @foreach($person['sosmed'] as $item)
                            @if($item->key == "facebook")
                                <a href="" class="social__item">
                                    <i class="seoicon-social-facebook"></i>
                                </a>
                            @endif
                            @if($item->key == "instagram")
                                <a href="" class="social__item">
                                    <i class="seoicon-social-instagram"></i>
                                </a>
                            @endif
                            @if($item->key == "twitter")
                                <a href="" class="social__item">
                                    <i class="seoicon-social-twitter"></i>
                                </a>
                            @endif
                            @if($item->key == "linkedin")
                                <a href="" class="social__item">
                                    <i class="seoicon-social-linkedin"></i>
                                </a>
                            @endif
                            @if($item->key == "google")
                                <a href="" class="social__item">
                                    <i class="seoicon-social-google-plus"></i>
                                </a>
                            @endif
                            @if($item->key == "youtube")
                                <a href="" class="social__item">
                                    <i class="seoicon-youtube"></i>
                                </a>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="product-description">
                <div class="container">

                    <ul class="product-description-control" role="tablist">

                        <li role="presentation" class="active">
                            <a href="#product-description" role="tab" data-toggle="tab" class="description control-item">Portofolio</a>
                        </li>

                        <li role="presentation">
                            <a href="#product-reviews" role="tab" data-toggle="tab" class="reviews control-item c-secondary">+</a>
                        </li>

                    </ul>

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="product-description">
                            <div class="row">
                                <div class="col-12">
                                    <div class="cart-main">
                                        <table class="shop_table cart mb-0">
                                            <thead class="cart-product-wrap-title-main">
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Judul Portofolio</th>
                                                    <th>Tahun</th>
                                                </tr>
                                            </thead>
                                            @php
                                                $i=1
                                            @endphp
                                            <tbody>
                                                @foreach($person['personPorto'] as $item)
                                                    <tr class="cart_item">
                                                        <td>{{ $i++ }}</td>
                                                        <td><h6>{{ $item->name }}</h6></td>
                                                        <td><h6 class="text-bold-800">{{ $item->year }}</h6></td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
