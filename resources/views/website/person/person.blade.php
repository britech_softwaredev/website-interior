@extends('website.layout.app')
@section('content')
    <div class="stunning-header" style="padding: 56px 0"></div>

    <div class="container">
        <div class="row medium-padding100">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="heading align-center">
                    <div class="h2 heading-title text-bold-800 mb-0">Developer Britech</div>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>
                    <p class="heading-text">Beberapa developer tergabung dalam masing-masing divisi bidang pekerjaan dengan kemampuan dan pengetahuannya yang dikuasai.
                    </p>
                </div>
            </div>
        </div>
        <div class="row pb120">
            @foreach($person as $item)
                <div class="col-lg-3 col-xs-12 sorting-item ecommerce smm">
                    <div class=" box-shadow-2 border-1">
                        <div class="client-image">
                            <div style="width: 100%;
                                height: 300px;
                                background-position: center top;
                                background-repeat: no-repeat;background-image: url('{{ $item->getFirstMediaUrl('default') }}')" class="br-top"></div>
                        </div>
                        <div class="client-item-style2 bg-border-color mb30 p-2 br-bottom">
                            <h5 class="clients-item-title text-bold-800 font-medium-3 text-capitalize mb-0">{{ explode(' ', trim($item->name ))[0] }} {{ explode(' ', trim($item->name ))[1] }}</h5>
                            <p class="clients-item-text" style="height: 70px">{{ $item['jabatan']['name'] }}</p>

                            <a href="{{ url('person').'/'.$item->slug }}" class="btn btn-small btn-border c-primary">
                                <span class="text font-small-2">PROFIL</span>
                                <span class="semicircle"></span>
                                <i class="seoicon-right-arrow"></i>
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection
