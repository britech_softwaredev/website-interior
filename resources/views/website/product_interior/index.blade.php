@extends('website.layout.app')
@section('content')

<section class="white-bg masonry-main page-section-ptb">
    <div class="container">
{{--        <div class="row">--}}
{{--            <div class="col-lg-12 col-md-12">--}}
{{--                <div class="section-title text-left">--}}
{{--                    <h2 class="title-effect">Furniture</h2>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="row">
            @foreach($product as $item)
                <div class="col-lg-12 col-md-12 mt-40">
                    <div class="pt-0 mb-50">
                        <h2 class="text-brown">{{ $item->title }}</h2>
                        <div class="font-small-3">{{ $item->description }}</div>
                        <div class="divider dashed mt-30"></div>
                    </div>
                </div>
                @foreach($item['komersils'] as $items)
                    <div class="col-md-2 col-sm-3 xs-mb-30 mb-3">
                        <div class="blog blog-simple text-start">
                            <div class="blog-image">
                                <img class="img-fluid" src="{{ url($items->getFirstMediaUrl()) }}" alt="" style="height: 140px">
                            </div>
                            <div class="blog-name mt-10 p-2">
                                <span class="mt-0"><a href="#" class="text-black text-bold-600 font-small-4 desc-p">{{ $items->title }}</a></span>
                                <div class="admin">
                                    <div class="font-small-2">{{ $items->color }}</div>
                                    <div class="font-small-2">{{ $items->dimensions }}</div>
                                    <ins class="text-black">Rp. {{ number_format($items->price,0,',','.')  }}</ins><br>
                                    <a href="{{ url('order/'.$items->id) }}" class="btn btn-sm btn-info mt-10 font-small-2" style="width: 100%">+ Keranjang</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endforeach
{{--            <div class="col-lg-12 col-md-12">--}}
{{--                <div class="masonry columns-3 popup-gallery">--}}
{{--                    <div class="grid-sizer"></div>--}}
{{--                    <div class="masonry-item photography illustration">--}}
{{--                        <div class="portfolio-item">--}}
{{--                            <img src="{{ asset('gambar interior/ruang tamu/135416273_1496245327252356_8758085860964363766_n.jpg') }}" alt="">--}}
{{--                            <div class="portfolio-overlay">--}}
{{--                                <h4 class="text-white"> <a href="portfolio-single-01.html"> Cari Desain Interior Kesukaan Anda Disini </a> </h4>--}}
{{--                                <span class="text-white"> <a href="#"> Pajangan Dan Hiasan </a> | <a href="#"> Ruang Tamu </a> </span>--}}
{{--                            </div>--}}
{{--                            <a class="popup portfolio-img " href="{{ asset('gambar interior/ruang tamu/135416273_1496245327252356_8758085860964363766_n.jpg') }}"><i class="fa fa-arrows-alt"></i></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="masonry-item photography">--}}
{{--                        <div class="portfolio-item">--}}
{{--                            <img src="{{ asset('gambar interior/ruang tamu/122388842_1739696976197026_504381382571695759_n.jpg') }}" alt="">--}}
{{--                            <div class="portfolio-overlay">--}}
{{--                                <h4 class="text-white"> <a href="portfolio-single-01.html"> Cari Desain Interior Kesukaan Anda Disini </a> </h4>--}}
{{--                                <span class="text-white"> <a href="#"> Laci </a> | <a href="#"> Ruang Tamu </a> </span>--}}
{{--                            </div>--}}
{{--                            <a class="popup portfolio-img" href="{{ asset('gambar interior/ruang tamu/122388842_1739696976197026_504381382571695759_n.jpg') }}"><i class="fa fa-arrows-alt"></i></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="masonry-item photography branding">--}}
{{--                        <div class="portfolio-item">--}}
{{--                            <img src="{{ asset('gambar interior/room_kamar/91602441_642199483013583_3450928454203223912_n.jpg') }}" alt="">--}}
{{--                            <div class="portfolio-overlay">--}}
{{--                                <h4 class="text-white"> <a href="portfolio-single-01.html"> Cari Desain Interior Kesukaan Anda Disini  </a> </h4>--}}
{{--                                <span class="text-white"> <a href="#"> Lemari Tanam </a> | <a href="#"> Kamar Design </a> </span>--}}
{{--                            </div>--}}
{{--                            <a class="popup portfolio-img" href="gambar interior/room_kamar/91602441_642199483013583_3450928454203223912_n.jpg"><i class="fa fa-arrows-alt"></i></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="masonry-item photography branding">--}}
{{--                        <div class="portfolio-item">--}}
{{--                            <img src="{{ asset('gambar interior/room dapur/92348925_232365684796997_383132563071527156_n.jpg') }}" alt="">--}}
{{--                            <div class="portfolio-overlay">--}}
{{--                                <h4 class="text-white"> <a href="portfolio-single-01.html"> Cari Desain Interior Kesukaan Anda Disini </a> </h4>--}}
{{--                                <span class="text-white"> <a href="#"> Kitchen Set </a> | <a href="#"> Dapur Design </a> </span>--}}
{{--                            </div>--}}
{{--                            <a class="popup portfolio-img" href="gambar interior/room dapur/92348925_232365684796997_383132563071527156_n.jpg"><i class="fa fa-arrows-alt"></i></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
    </div>
</section>

@endsection
