@extends('website.layout.app')
@section('content')
    @if($order != null && count($order['detailOrders']) > 0)
        <section class="wishlist-page page-section-ptb">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Product name</th>
                                    <th>Price</th>
                                    <th>Chat </th>
                                    <th>Close </th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($order))
                                    @php
                                        $total = 0;
                                    @endphp
                                    @foreach($order['detailOrders'] as $item)
                                        <tr>
                                            <td class="image">
                                                <a class="media-link" href="#"> <img class="img-fluid width-100" src="{{ url($item['product']->getFirstMediaUrl()) }}" alt=""/></a>
                                            </td>
                                            <td class="description">
                                                <a href="#">{{ $item['product']['title'] }}</a>
                                            </td>
                                            <td class="price">Rp. {{ number_format($item['product']->price,0,',','.')  }}</td>
                                            <td>
                                                <a href="https://wa.me/6283140141152?text=Hai,admin%20apakah%20barang%20ini%20tersedia%20?%20{{ $item['product']['title'] }}%20Rp.{{ number_format( $item['product']['price'],0,',','.')  }}%20{{ url('product') }}"><i class="fa fa-whatsapp theme-color font-medium-1"></i></a>
                                            </td>
                                            <td class="total">
                                                {!! Form::open(['route' => ['detailOrders.destroy', $item->id], 'method' => 'delete']) !!}
                                                {!! Form::button('<i class="fa fa-close"></i>', ['type' => 'submit', 'class' => 'btn btn-sm btn-danger']) !!}
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                        @php
                                            $total += $item['product']->price
                                        @endphp
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="checkout-page">
            <div class="container">
                <form action="{{ url('postOrder/'.$order->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <h3 class="mb-20">Order notes</h3>
                            <div class="section-field mb-30">
                                {!! Form::textarea('note', null, ['class' => 'form-control','rows'=> '7']) !!}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="gray-bg pl-50 pr-50 pt-40 pb-50">
                                <table class="mb-30">
                                    <tbody>
                                    <tr>
                                        <th class="pl-40"><h4>GRAND TOTAL</h4> </th>
                                        <td class="pl-40"><h4>
                                                Rp. {{ number_format($total,0,',','.')  }}
                                            </h4></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <button type="submit" class="button btn-block">Pesan Sekarang <span class="icon-action-redo"></span></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    @else
        <section class="page-section white-bg page-section-ptb">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 pb-20 mt-50">
                        <div class="card-body pt-0 text-center">
                            <i class="fa fa-shopping-basket font-large-5 text-silver mb-20"></i>
                            <h1 class="text-black">Tidak Ada Furniture Dalam Keranjang</h1>
                            <div class="divider dashed mt-30"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif
@endsection
