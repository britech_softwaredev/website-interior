<div class="modal fade pl-30 pr-30" id="modal-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-{{ $item->id }}" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route' => 'buktiPembayarans.store'], ['class' => 'form']) !!}
            <div class="modal-title" id="exampleModalLabel">
                <div class="section-title mt-30 mb-0">
                    <h5 class="text-uppercase text-center">Upload Bukti Transfer</h5>
                    <div class="divider small mt-10"></div>
                </div>
            </div>
                @if(empty($item['pembayaran']) && \Carbon\Carbon::parse($item->created_at)->format('d/m/Y') == \Carbon\Carbon::now()->format('d/m/Y'))
                    <div class="modal-body">
                        @foreach($rekening as $items)
                            <div class="text-black">
                                {!! $items->deskripsi !!}
                            </div>
                        @endforeach
                        <div class="divider dashed mt-20"></div>
                        <div class="theme-color mt-20 text-bold-700"><i class="fa fa-map-pin"></i> Alamat Lengkap Tujuan Pengantaran</div>
                        <textarea class="form-control" name="deskripsi" placeholder="Alamat lengkap"></textarea>
                        <div class="theme-color mt-20 text-bold-700"><i class="fa fa-credit-card"></i> Foto Bukti Pembayaran</div>
                        <x-media-library-attachment name="media" rules="mimes:png,jpeg"/>
                        <input type="text" name="order_product_id" hidden value="{{ $item->id }}">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-success">Unggah</button>
                    </div>
                @elseif(!empty($item['pembayaran']))
                    <div class="modal-body">
                        <div class="theme-color mt-20 text-bold-700"><i class="fa fa-map-pin"></i> Alamat Pengantaran</div>
                        <p class="text-black">{{ $item['pembayaran']->deskripsi ?? '-' }}</p>
                        <div class="theme-color mt-20 text-bold-700"><i class="fa fa-credit-card"></i> Foto Bukti Pembayaran</div>
                        <div class="text-black mb-10">{{ \Carbon\Carbon::parse($item['pembayaran']->created_at)->format('d F Y - h:i') }}</div>
                        <div class="text-center">
                            <img src="{{ url($item['pembayaran']->getFirstMediaUrl('default')) }}" alt="" class="img-fluid">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                @else
                    <div class="modal-body">
                        <div class="theme-color mt-20 text-bold-700"><i class="fa fa-credit-card"></i> Foto Bukti Pembayaran</div>
                        <div class="text-black mb-10">Tidak ada bukti pembayaran</div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                @endif
            {!! Form::close() !!}
        </div>
    </div>
</div>
