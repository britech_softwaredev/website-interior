@extends('website.layout.app')
@section('content')
<section class="rev-slider o-hidden">
  <div id="rev_slider_261_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="slider-interior-design" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
    <!-- START REVOLUTION SLIDER 5.4.6.3 auto mode -->
      <div id="rev_slider_261_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.6.3">
            <ul>  <!-- SLIDE  -->
                <li data-index="rs-744" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb=" {{asset('image/banner_exp/slide2.jpeg')}}"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                <!-- MAIN IMAGE -->
                    <img src=" {{asset('gambar interior/empty-living-room-with-blue-sofa-plants-table-empty-white-wall-background-3d-rendering.jpg')}}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->

                <!-- LAYER NR. 1 -->
                <div class="tp-caption   tp-resizeme"
                     id="slide-744-layer-1"
                     data-x="395"
                     data-y="center" data-voffset="-120"
                     data-width="['auto']"
                     data-height="['auto']"
                     data-type="text"
                     data-responsive_offset="on"

                        data-frames='[{"delay":600,"speed":2000,"sfxcolor":"#ffffff","sfx_effect":"blockfromtop","frame":"0","from":"z:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                        data-textAlign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[10,10,10,10]"
                        data-paddingright="[20,20,20,20]"
                        data-paddingbottom="[10,10,10,10]"
                        data-paddingleft="[20,20,20,20]"

                        style="z-index: 5; white-space: nowrap; font-size: 18px; line-height: 18px; font-weight: 600; color: #323232; letter-spacing: 4px;text-transform:uppercase;background-color:rgb(255,255,255);">haii welcome to Royan Interior</div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption   tp-resizeme"
                   id="slide-744-layer-2"
                   data-x="393"
                   data-y="413"
                        data-width="['auto']"
                  data-height="['auto']"

                        data-type="text"
                  data-responsive_offset="on"

                        data-frames='[{"delay":1880,"speed":2000,"sfxcolor":"#ffffff","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                        data-textAlign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[10,10,10,10]"
                        data-paddingright="[20,20,20,20]"
                        data-paddingbottom="[10,10,10,10]"
                        data-paddingleft="[20,20,20,20]"

                        style="z-index: 6; white-space: nowrap; font-size: 72px; line-height: 72px; font-weight: 400; color: #ffffff; letter-spacing: 4px;text-transform:uppercase;background-color:rgb(50,50,50);">Rinaroyan siap membantu anda</div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption   tp-resizeme  rev-bg"
                   id="slide-744-layer-3"
                   data-x="395"
                   data-y="517"
                     data-width="['auto']"
                  data-height="['auto']"

                        data-type="text"
                  data-responsive_offset="on"

                        data-frames='[{"delay":2860,"speed":2000,"sfxcolor":"#ffffff","sfx_effect":"blockfromright","frame":"0","from":"z:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                        data-textAlign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[10,10,10,10]"
                        data-paddingright="[20,20,20,20]"
                        data-paddingbottom="[10,10,10,10]"
                        data-paddingleft="[20,20,20,20]"

                        style="z-index: 7; white-space: nowrap; font-size: 72px; line-height: 72px; font-weight: 400; color: #ffffff; letter-spacing: 4px;text-transform:uppercase;background-color:rgb(186,137,63);">Mengdesain Interior </div>

                <!-- LAYER NR. 4 -->
              </li>
              <li data-index="rs-744" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb=" {{asset('image/banner_exp/slide2.jpeg')}}" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE 2-->
                    <img src=" {{asset('gambar interior/gambar beranda slide 2.jpg')}}" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->

                </li>
            </ul>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
      </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-5 page-section-ptb pl-20 pr-20">
                <div class="section-title">
                    <h6 class="text-bold-400 mb-30">Gaya Interior?</h6>
                    <h2 class="text-black font-large-1">Masih mencari design? Kami akan membantu Anda menemukan gaya Anda!</h2>
                    <p class="mt-30 text-black font-medium-1">Biarkan kami melakukan yang terbaik – <br>mengenal Anda dan gaya unik Anda.</p>
                </div>
                <div class="mt-30">
                    <button type="button" class="button icon mb-10 mr-10 text-capitalize" data-toggle="modal" data-target="#exampleModal">
                        Jelajah Galeri<i class="fa fa-rocket"></i>
                    </button>
                </div>
            </div>
            <div class="col-lg-7 sm-mb-30 align-self-center">
                <div class="owl-carousel bottom-center-dots" data-nav-dots="false" data-smartspeed="1200" data-items="3" data-md-items="1" data-sm-items="1" data-xs-items="1" data-xx-items="1">
                    <!-Item 1-->
                    <div class="item">
                        <img class="rounded-1" src="{{ asset('gambar interior/interior hiasan/91707012_514489755882872_5456072366786421858_n.jpg') }}" alt="">
                        <h6 class="text-bold-400 mt-20">Hiasan</h6>
                    </div>
                    <!-Item 2-->
                    <div class="item">
                        <img class="rounded-1" src="{{ asset('gambar interior/interior lantai/120924054_279330180161243_1951144313964466519_n.jpg') }}" alt="">
                        <h6 class="text-bold-400 mt-20">Lantai</h6>
                    </div>
                    <!-Item 3-->
                    <div class="item">
                        <img class="rounded-1" src="{{ asset('gambar interior/room dapur/151887360_174881524174724_7221468876282200_n.jpg') }}" alt="">
                        <h6 class="text-bold-400 mt-20">Dapur</h6>
                    </div>
                    <!-Item 4-->
                    <div class="item">
                        <img class="rounded-1" src="{{ asset('gambar interior\room_kamar\118176511_177470447152394_949054435360239833_n.jpg') }}" alt="">
                        <h6 class="text-bold-400 mt-20">Kamar</h6>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="split-section brown-bg page-section-ptb">
    <div class="side-background">
        <div class="col-lg-6 img-side img-left">
            <div class="img-holder img-cover" data-jarallax='{"speed": 0.6}' style="background-image: url('{{ asset('gambar interior/room dapur/151887360_174881524174724_7221468876282200_n.jpg') }}');">
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-end">
            <div class="col-lg-5">
                <div class="section-title">
                    <h6 class="text-brown mb-20">Vendor Interior</h6>
                    <h2 class="text-black font-large-2">Kami bekerja sama dengan beberapa vendor besar dan kecil.</h2>
                    <p class="text-black font-medium-1 mt-30">
                        Desainer kami dengan cermat menyediakan pilihan produk, hanya untuk Anda. Kami bermitra dengan daftar lengkap merek yang dicari, pengecer yang sedang naik daun, dan pengrajin lokal untuk memastikan bahwa setiap desain disesuaikan secara unik dengan gaya, kebutuhan, dan anggaran Anda.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="our-services gray-bg pb-40">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 page-section-ptb pl-20 pr-20 pb-0 text-center">
                <h1 class="text-black">Sosial Media</h1>
            </div>
            <div class="col-lg-12">
            <div class="embedsocial-hashtag" data-ref="95697834e40b0439b92fb6235291983e61da4d9d"> <a class="feed-powered-by-es feed-powered-by-es-feed-new" href="https://embedsocial.com/social-media-aggregator/" target="_blank" title="Widget by EmbedSocial"> Widget by EmbedSocial<span>→</span> </a> </div> <script> (function(d, s, id) { var js; if (d.getElementById(id)) {return;} js = d.createElement(s); js.id = id; js.src = "https://embedsocial.com/cdn/ht.js"; d.getElementsByTagName("head")[0].appendChild(js); }(document, "script", "EmbedSocialHashtagScript")); </script>
        </div>
        </div>
    </div>
</section>

@endsection
{{--@section('css')--}}
{{--    <link type="text/css" rel="stylesheet" href="{!! asset('css/ycp.css') !!}" />--}}

{{--    <div id="fb-root"></div>--}}
{{--    <div id="fb-root"></div>--}}
{{--    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v10.0&appId=298637494983635&autoLogAppEvents=1" nonce="hYx81EtD"></script>--}}

{{--    <script async src="https://platform.instagram.com/widgets.js" charset="utf-8"></script>--}}
{{--@endsection--}}
{{--@section('scripts')--}}
{{--    <!-- jQuery -->--}}
{{--    <script src="{!! asset('js/ycp.js') !!}"></script>--}}
{{--    <script>--}}
{{--        $(function() {--}}

{{--            $("#channel-dprd-kaltim").ycp({--}}
{{--                apikey : 'AIzaSyBy_wcF4Lnkd9uigQCuJaRNUNtgQ6RIXdI',--}}
{{--                playlist : 7,--}}
{{--                autoplay : false,--}}
{{--                related : true--}}
{{--            });--}}

{{--        });--}}
{{--    </script>--}}
{{--@endsection--}}
