@extends('website.layout.app')
@section('content')

<section class="page-title bg-overlay-black-50 parallax " data-jarallax='{"speed": 0.6}' style="background-image: url('{{ asset('gambar interior/toilet/kamar-mandi-rumah-minimalis_169.jpeg') }}');">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 pb-20">
                <div class="page-title-name">
                    <h1>Details Ruangan Toilet</h1>
                    <p>TEMUKAN BARANG KESUKAAN ANDA.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="page-section white-bg o-hidden">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12 ">
                        <div class="card-body card-dasboard pb-2">
                            <div class="section-title line-dabble pt-3">
                                <h4 class="title"><b>Detail Barang</b></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="shop-06-product page-section-pb">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/toilet/120745259_333381924580681_8202486878976353638_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10 p-2">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">After Bifore interior</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/toilet/120516663_338921013860034_6066696731431003575_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Furniture Dinding Mini</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/toilet/120490547_627346301283857_8530756479850619623_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Sower toilet</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/toilet/120393420_801676650374473_7108547521808428975_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Sower Dan Furnitur Dinding</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection