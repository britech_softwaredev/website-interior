@extends('website.layout.app')
@section('content')

<section class="page-title bg-overlay-black-50 parallax " data-jarallax='{"speed": 0.6}' style="background-image: url('{{ asset('gambar interior/room dapur/120672356_179389767013690_2659392587651096518_n.jpg') }}');">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 pb-20">
                <div class="page-title-name">
                    <h1>Details Ruangan Dapur</h1>
                    <p>TEMUKAN BARANG KESUKAAN ANDA.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="page-section white-bg o-hidden">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12 ">
                        <div class="card-body card-dasboard pb-2">
                            <div class="section-title line-dabble pt-3">
                                <h4 class="title"><b>Detail Barang</b></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="shop-06-product page-section-pb">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room dapur/122357922_673393983293697_2661481297619413791_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10 p-2">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Kitchen One Set</a></span>
                        <div class="admin">
                            <span class="font-small-1">lemari dinding dapur mini</span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room dapur/122215628_2061044334025583_8915529708207218620_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">kitchen One Set</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room dapur/122046920_105766267991762_7065815559170121893_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Kitchen One set</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room dapur/151887360_174881524174724_7221468876282200_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Bar Dapur</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
             <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room dapur/151387933_778620289704990_7270130024734995677_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Bar Dapur</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room dapur/121662530_1327943637543402_6161508269990438681_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Kitchen One Set Big</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room dapur/121595316_647751119255763_4253175180413321823_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Kitchen One Set Big</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room dapur/121617000_218574609598377_6689209349956917678_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Kitchen Set</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room dapur/121589384_123779822584189_1737243921264413804_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Kitchen Set</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room dapur/120672356_179389767013690_2659392587651096518_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Kitchen Set Full Dapur</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room dapur/120502641_1203653230034266_1494668040442305525_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Kitchen Set Full Dapur</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection