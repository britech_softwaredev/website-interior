@extends('website.layout.app')
@section('content')

<section class="page-title parallax" style="background-image: url('{{ $interior->getFirstMediaUrl() }}'); height: 600px;">
</section>

<section class="page-section white-bg">
    <div class="container">
        <div class="row">
            <div class="col-12 text-right mt-40 pr-30">
                <a href="{{ url('favoritesAdd/'.$interior->id) }}"><i class="fa fa-heart font-large-2 text-brown"></i></a>
            </div>
            <div class="col-lg-12 pb-20">
                <div class="card-body pt-0">
                    <h4 class="text-brown">{{ $interior['categoryInterior']['title']}}</h4>
                    <h1 class="text-black">{{ $interior->title }}</h1>
                    <div>{{ $interior->description }}</div>
                    <p>{{ $interior->dimensions }}</p>
                    <h1 class="text-brown text-bold-800">Rp. {{ number_format($interior->price,0,',','.')  }}</h1>
                    <div class="divider dashed mt-30"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="shop-06-product mt-30">
    <div class="container">
        <div class="row">
            @foreach($interior['products'] as $item)
                <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                    <div class="blog blog-simple text-start">
                        <div class="blog-image">
                            <img class="img-fluid" src="{{ url($item->getFirstMediaUrl()) }}" alt="" style="height: 200px">
                        </div>
                        <div class="blog-name mt-10 p-2">
                            <span class="mt-0"><a href="#" class="text-black text-bold-600 font-medium-1">{{ $item->title }}</a></span>
                            <div class="admin">
                                <div class="font-small-3">{{ $item->color }}</div>
                                <div class="font-small-3">{{ $item->dimensions }}</div>
{{--                                <ins class="text-black">Rp. {{ number_format($item->price,0,',','.')  }}</ins>--}}
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row mt-40">
            <div class="col-12">
                <div class="gray-bg pl-50 pr-50 pt-50 pb-50 text-center">
                    <h4>Proses Pemesanan Dapat Langsung Menghubungi</h4>
                    <a href="https://wa.me/6283140141152" class="button d-grid mt-10" >Customer Service <span class="icon-action-redo"></span></a>
                </div>
            </div>
        </div>
{{--            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">--}}
{{--                <div class="blog blog-simple text-start">--}}
{{--                    <div class="blog-image">--}}
{{--                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/135297880_2865685090309638_7883525477695375176_n.jpg') }}" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="blog-name mt-10 p-2">--}}
{{--                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Furniture Dinding</a></span>--}}
{{--                        <div class="admin">--}}
{{--                            <span class="font-small-1">hiasan tempel untuk menyimpan</span>--}}
{{--                            <ins class="text-black">$24.99</ins>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">--}}
{{--                <div class="blog blog-simple text-start">--}}
{{--                    <div class="blog-image">--}}
{{--                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/135256105_3444731955624652_5564764060966389881_n.jpg') }}" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="blog-name mt-10">--}}
{{--                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Kitchen Set Mini </a></span>--}}
{{--                        <div class="admin">--}}
{{--                            <span class="font-small-1">peralatan dan perlengkapan untuk menyimpan</span>--}}
{{--                            <ins class="text-black">$24.99</ins>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-md-3 col-sm-3">--}}
{{--                <div class="blog blog-simple text-start">--}}
{{--                    <div class="blog-image">--}}
{{--                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/135054302_211720397204321_4743514517207703122_n.jpg') }}" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="blog-name mt-10">--}}
{{--                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Tempat Tidur Tingkat</a></span>--}}
{{--                        <div class="admin">--}}
{{--                        <span class="font-small-1"></span>--}}
{{--                            <ins class="text-black">$24.99</ins>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">--}}
{{--                <div class="blog blog-simple text-start">--}}
{{--                    <div class="blog-image">--}}
{{--                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/134984095_718256975501251_2933970345479010237_n.jpg') }}" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="blog-name mt-10">--}}
{{--                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Lemari 2(Dua)  Pintu Tanam</a></span>--}}
{{--                        <div class="admin">--}}
{{--                        <span class="font-small-1"></span>--}}
{{--                            <span class="font-small-1">peralatan dan penyimpanan</span>--}}
{{--                            <ins class="text-black">$24.99</ins>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">--}}
{{--                <div class="blog blog-simple text-start">--}}
{{--                    <div class="blog-image">--}}
{{--                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/120840063_622923768395051_7883409408562430976_n.jpg') }}" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="blog-name mt-10">--}}
{{--                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Tempat Tidur </a></span>--}}
{{--                        <div class="admin">--}}
{{--                        <span class="font-small-1"></span>--}}
{{--                            <ins class="text-black">$24.99</ins>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">--}}
{{--                <div class="blog blog-simple text-start">--}}
{{--                    <div class="blog-image">--}}
{{--                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/120747579_360567735096601_2571556838499374908_n.jpg') }}" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="blog-name mt-10">--}}
{{--                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Tempat tidur Laci</a></span>--}}
{{--                        <div class="admin">--}}
{{--                            <span class="font-small-1">peralatan dan penyimpanan barang</span>--}}
{{--                            <ins class="text-black">$24.99</ins>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">--}}
{{--                <div class="blog blog-simple text-start">--}}
{{--                    <div class="blog-image">--}}
{{--                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/120603686_351247485930135_5854102884158062924_n.jpg') }}" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="blog-name mt-10">--}}
{{--                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Furniture Dinding Dan Lampu Kamar </a></span>--}}
{{--                        <div class="admin">--}}
{{--                        <span class="font-small-1"></span>--}}
{{--                            <ins class="text-black">$24.99</ins>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">--}}
{{--                <div class="blog blog-simple text-start">--}}
{{--                    <div class="blog-image">--}}
{{--                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/91569528_556032488378411_501962937575033718_n.jpg') }}" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="blog-name mt-10">--}}
{{--                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Kamar Simple Dan Menarik</a></span>--}}
{{--                        <div class="admin">--}}
{{--                            <span class="font-small-1"></span>--}}
{{--                            <ins class="text-black">$24.99</ins>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
    </div>
</section>


@endsection
