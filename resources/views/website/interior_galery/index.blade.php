@extends('website.layout.app')
@section('content')

<section class="page-title left parallax bg-overlay-black-40" style="background-image: url('{{asset('gambar interior/gambar beranda 2.jpg')}}'); height: 500px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                 <div class="page-title-name">
                    <h1>Ragam Design Interior</h1>
                    <p>Sesuaikan Dengan Kebutuhan Rumah</p>
                </div>
            </div>
        </div>
    </div>
</section>
{{--<section class="page-section white-bg">--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="col-lg-12 mt-30">--}}
{{--                <div class="card-body text-center">--}}
{{--                    <h1 class="text-black text-bold-800">Ragam Design Interior</h1>--}}
{{--                    <h6 class="text-brown text-bold-400">Sesuaikan Dengan Kebutuhan Rumah</h6>--}}
{{--                    <div class="divider"></div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}
<section class="shop-06-product mt-50">
    <div class="container">
{{--        <div class="row">--}}
{{--            <div class="col-lg-12 col-md-12">--}}
{{--                <div class="section-title text-center">--}}
{{--                    <h2 class="title-effect">New Collection</h2>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

        <div class="row">
            @foreach($interior as $item)
                <div class="col-md-4 col-sm-3 xs-mb-30 mb-3">
                    <div class="blog blog-simple text-start rounded">
                        <div class="blog-image">
                            <img class="img-fluid" src="{{ url($item->getFirstMediaUrl()) }}" alt="">
                        </div>
                        <div class="blog-name mt-20">
                            <h6 class="text-brown mt-15">{{ $item['categoryInterior']['title'] }}</h6>
                            <h4><a href="{{ ('interior/'.$item->slug) }}">{{ $item->title }}</a></h4>
                        </div>
                    </div>
                </div>
            @endforeach
{{--            --}}
{{--            <div class="col-md-4 col-sm-4 xs-mb-30 mb-3">--}}
{{--                <div class="blog blog-simple text-start">--}}
{{--                    <div class="blog-image">--}}
{{--                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/133826506_217155126695563_2212166765703250833_n.jpg') }}" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="blog-name mt-20">--}}
{{--                        <h5 class="mt-15"><a href="{{url('interior_galery/detail')}}">KAMAR ANAK</a></h5>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-md-4 col-sm-4 xs-mb-30 mb-3">--}}
{{--                <div class="blog blog-simple text-start">--}}
{{--                    <div class="blog-image">--}}
{{--                        <img class="img-fluid" src="{{ asset('gambar interior/ruang keluarga/120472415_2784336075184220_1472530637959679424_n.jpg') }}" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="blog-name mt-20">--}}
{{--                        <h5 class="mt-15"><a href="{{url('interior_galery/detail2')}}">RUANG KELUARGA</a></h5>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-md-4 col-sm-4">--}}
{{--                <div class="blog blog-simple text-start">--}}
{{--                    <div class="blog-image">--}}
{{--                        <img class="img-fluid" src="{{ asset('gambar interior/room dapur/122215628_2061044334025583_8915529708207218620_n.jpg') }}" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="blog-name mt-20">--}}
{{--                        <h5 class="mt-15"><a href="{{url('interior_galery/detail3')}}">RUANG DAPUR</a></h4>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-md-4 col-sm-4 xs-mb-30 mb-3">--}}
{{--                <div class="blog blog-simple text-start">--}}
{{--                    <div class="blog-image">--}}
{{--                        <img class="img-fluid" src="{{ asset('gambar interior/ruang tamu/117131708_163938025221041_3439651213682972366_n.jpg') }}" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="blog-name mt-20">--}}
{{--                        <h5 class="mt-15"><a href="{{url('interior_galery/detail4')}}">RUANG TAMU</a></h5>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-md-4 col-sm-4 xs-mb-30 mb-3">--}}
{{--                <div class="blog blog-simple text-start">--}}
{{--                    <div class="blog-image">--}}
{{--                        <img class="img-fluid" src="{{ asset('gambar interior/toilet/120393420_801676650374473_7108547521808428975_n.jpg') }}" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="blog-name mt-20">--}}
{{--                        <h5 class="mt-15"><a href="{{url('interior_galery/detail5')}}">TOILET</a></h5>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-md-4 col-sm-4 xs-mb-30 mb-3">--}}
{{--                <div class="blog blog-simple text-start">--}}
{{--                    <div class="blog-image">--}}
{{--                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/118176511_177470447152394_949054435360239833_n.jpg') }}" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="blog-name mt-20">--}}
{{--                        <h5 class="mt-15"><a href="{{url('interior_galery/detail6')}}">RUANG KAMAR</a></h5>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
    </div>
</section>

@endsection
