@extends('website.layout.app')
@section('content')

<section class="page-title bg-overlay-black-50 parallax " data-jarallax='{"speed": 0.6}' style="background-image: url('{{ asset('gambar interior/room_kamar/118122352_601047010585506_1677732725539264572_n.jpg') }}');">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 pb-20">
                <div class="page-title-name">
                    <h1>Details Ruangan kamar</h1>
                    <p>TEMUKAN BARANG KESUKAAN ANDA.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="page-section white-bg o-hidden">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12 ">
                        <div class="card-body card-dasboard pb-2">
                            <div class="section-title line-dabble pt-3">
                                <h4 class="title"><b>Detail Barang</b></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="shop-06-product page-section-pb">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/118176511_177470447152394_949054435360239833_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10 p-2">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Full Set Interior </a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/118122352_601047010585506_1677732725539264572_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Kamar Full Set Interior</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/117887031_223384372404721_6479763897780975411_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Lemari Full Set</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/118233180_930702840770698_8056098891312836653_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Lemari Pinnk</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/118199291_313895293264576_1694230867967368208_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Lemari TV Pink Kamar</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/117518850_337251537320188_7317522125505621360_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Lemari Pink</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/91523823_641141006453143_2841135072607502610_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Kasur classic</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/91465388_2451226628311711_6519557410166208957_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Kasur Classic</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/91480289_214682139757220_8897783360462871096_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Kamar Full Set Interior</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/91542398_665107884291001_7875759627586477036_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Kamar Full Set Interior</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/92652837_207944823960338_1242037186319704459_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Kasur Gold</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/92704638_556362724993728_868845765460073627_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Kasur Gold</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/91525992_242789870236457_9134179037765223007_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Kasur Ungu Minimalis</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/91624262_214421093215084_5499715301241045302_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Lemari Ungu Dua(2)Pintu</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/91855534_240770963742325_7110744416645664958_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Lemari TV Minimalis</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/91852751_1553118594836131_8711451265404979094_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Kasur Minimalis  Biru</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/91818285_805860603240547_2055164093177874235_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Lemari  Biru Tv Minimalis</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/room_kamar/92227804_266348301058067_7579287464005536133_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black"> Lemari Biru (2) Pintu</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection