@extends('website.layout.app')
@section('content')

<section class="page-title bg-overlay-black-50 parallax " data-jarallax='{"speed": 0.6}' style="background-image: url('{{ asset('gambar interior/ruang keluarga/123137433_189470479380570_1488087646811875611_n.jpg') }}');">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 pb-20">
                <div class="page-title-name">
                    <h1>Details Ruangan Keluarga</h1>
                    <p>TEMUKAN BARANG KESUKAAN ANDA.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="page-section white-bg o-hidden">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12 ">
                        <div class="card-body card-dasboard pb-2">
                            <div class="section-title line-dabble pt-3">
                                <h4 class="title"><b>Detail Barang</b></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="shop-06-product page-section-pb">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/ruang keluarga/123205368_420242788971691_77605522552566988_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10 p-2">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Lemari TV</a></span>
                        <div class="admin">
                            <span class="font-small-1">peralatan dan penyimpanan</span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/ruang keluarga/123142108_130897055117833_9068555825411181609_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Hiasan Dinding Dan Pembatas Ruangan</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/ruang keluarga/117965555_300953791330569_4167197447562486072_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Tempat TV</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/ruang keluarga/117126088_1396563960540193_2230869116511702231_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Tempat TV Coklat</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/ruang keluarga/91717735_295330154784017_6379576598264412544_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Tempat Tv Mini</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/ruang keluarga/91552271_128373792094372_6391185909981230475_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Tempat TV Dinding</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/ruang keluarga/91470212_686563365420213_8404027474979771478_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Lemari TV Mini</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection