@extends('website.layout.app')
@section('content')

<section class="page-title bg-overlay-black-50 parallax " data-jarallax='{"speed": 0.6}' style="background-image: url('{{ asset('gambar interior/ruang tamu/135416273_1496245327252356_8758085860964363766_n.jpg') }}');">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 pb-20">
                <div class="page-title-name">
                    <h1>Details Ruangan Tamu</h1>
                    <p>TEMUKAN BARANG KESUKAAN ANDA.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="page-section white-bg o-hidden">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12 ">
                        <div class="card-body card-dasboard pb-2">
                            <div class="section-title line-dabble pt-3">
                                <h4 class="title"><b>Detail Barang</b></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="shop-06-product page-section-pb">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/ruang tamu/122810823_360141121799651_723522639850650163_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10 p-2">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Laci Mini</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/ruang tamu/122735362_267126404700502_5403784554840912236_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Laci Mini</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/ruang tamu/122734798_172174777876380_1734620397241260118_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Laci Mini</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/ruang tamu/122670974_628427587853273_1699375649985018277_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Laci Mini</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/ruang tamu/117235415_619446245377092_7809092668107591369_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Pembatas Ruangan</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 xs-mb-30 mb-3">
                <div class="blog blog-simple text-start">
                    <div class="blog-image">
                        <img class="img-fluid" src="{{ asset('gambar interior/ruang tamu/91547225_699801554104382_2203460835219424746_n.jpg') }}" alt="">
                    </div>
                    <div class="blog-name mt-10">
                        <span class="mt-0"><a href="#" class="text-black" class="text-black">Pembatas Ruangan</a></span>
                        <div class="admin">
                            <span class="font-small-1"></span>
                            <ins class="text-black">$24.99</ins>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection