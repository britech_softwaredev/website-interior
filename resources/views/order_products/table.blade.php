<!-- Sudah di modifikasi untuk Edit,Lihat,Hapus -->
<table class="table default">
    <thead>
    <tr class="text-uppercase bg-grey bg-lighten-4 black">
        <th>No.</th>
        <th>Kode & Status</th>
        <th>Data Pemesan</th>
        <th>Furniture Order</th>
        <th>Perubahan Status</th>
{{--        <th style="text-align: center">Action</th>--}}
    </tr>
    </thead>
    <tbody>
    @php
        $no = 1;
    @endphp
    @foreach($orderProducts as $orderProduct)
        <tr>
            <td>{!! $no++ !!}</td>
            <td>
                <span class="text-bold-600 black">ROYAN-{{ $orderProduct->id }}</span><br><br>
                <span class="{!! $orderProduct['status']['description'] !!}">{!! $orderProduct['status']['name'] !!}</span><br>
                <a href="" data-toggle="modal" data-target="#bukti-{{ $orderProduct->id }}" class="btn btn-sm btn-light mt-1 border-grey border-lighten-3 rounded-2 text-bold-600"><i class="fa fa-credit-card"></i> Bukti Transfer</a>
                @include('order_products.information')
            </td>
            <td>
                <div class="text-bold-600 black">{!! $orderProduct['users']['name'] !!}</div>
                <div class="black"><i class="fa fa-phone-square"></i> {!! $orderProduct['users']['kontak'] !!}</div>
                <div class="black">{!! $orderProduct['users']['email'] !!}</div>
            </td>
            <td>
                @php
                    $price = 0;
                @endphp
                @foreach($orderProduct['detailOrders'] as $item)
                    <div class="d-flex border-bottom-grey border-bottom-lighten-2 pb-0-1 pt-0-1">
                        <img src="{{ $item['product']->getFirstMediaUrl() }}" alt="furniture" class="img-fluid height-40 width-50">
                        <div>
                            <div class="ml-0-1 font-small-3 text-bold-600 black">{{ $item['product']['title'] }}</div>
                            <span class="ml-0-1 font-small-3">Rp.{{ number_format($item['product']['price'],0,',','.')  }}</span>
                        </div>
                    </div>
                    @php
                        $price += $item['product']['price']
                    @endphp
                @endforeach
                <h5 class="text-bold-800 text-right mt-0-1">Total : <span class="green">Rp. {{ number_format($price,0,',','.')  }}</span></h5>
            </td>
            <td>
{{--                <a href="{!! route('orderProducts.show', [$orderProduct->id]) !!}" class="btn btn-sm btn-info"><i class="fa fa-eye"></i></a>--}}
                @foreach($status as $statuses)
                    @if($statuses->id != $orderProduct->status_id && $statuses->name != "KERANJANG" && $statuses->name != "KONFIRMASI PEMBAYARAN")
                        <a href="{{ url('updateStatus/'.$orderProduct->id.'?idStatus='.$statuses->id) }}" class="btn btn-sm btn-info">{{ $statuses->name }}</a>
                    @endif
                @endforeach
                {!! Form::model($orderProduct, ['route' => ['orderProducts.update', $orderProduct->id], 'method' => 'patch','class'=>'text-right']) !!}
                    {!! Form::textarea('description_status', null, ['class' => 'form-control mt-0-1','rows'=>4]) !!}
                    {!! Form::submit('Simpan Note', ['class' => 'btn btn-sm btn-light']) !!}
                {!! Form::close() !!}
            </td>
{{--            <td>--}}
{{--                {!! Form::open(['route' => ['orderProducts.destroy', $orderProduct->id], 'method' => 'delete']) !!}--}}
{{--                <div class="btn-group" role="group" aria-label="Basic example">--}}
{{--                    <a href="{!! route('orderProducts.edit', [$orderProduct->id]) !!}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>--}}
{{--                       {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-sm btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}--}}
{{--                </div>--}}
{{--                {!! Form::close() !!}--}}
{{--            </td>--}}
        </tr>
    @endforeach
    </tbody>
</table>
