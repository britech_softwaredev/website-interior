<div class="modal fade text-left pl-3 pr-3" id="bukti-{{ $orderProduct->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Informasi Pembayaran</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="red">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if(!empty($orderProduct['pembayaran']))
                    <h5 class="black text-bold-700">Alamat Pengantaran</h5>
                    <p>{{ $orderProduct['pembayaran']->deskripsi }}</p>
                    <h5 class="black text-bold-700 mt-2">Bukti Transfer Pembayaran Barang</h5>
                    <div>{{ \Carbon\Carbon::parse($orderProduct['pembayaran']->created_at)->format('d F Y - h:i') }}</div>
                    <div class="text-center">
                        <img src="{{ url($orderProduct['pembayaran']->getFirstMediaUrl('default')) }}" alt="" class="img-fluid">
                    </div>
                @else
                    <div class="text-center mb-1"><i class="fa fa-credit-card font-large-2"></i></div>
                    <h5 class="black text-bold-700 text-center">Belum Ada Bukti Transfer Pembayaran Barang</h5>
                @endif
            </div>
        </div>
    </div>
</div>
