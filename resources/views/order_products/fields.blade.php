<!-- Users Id Field -->
<div class="form-group">
    {!! Form::label('users_id', 'Users Id:') !!}
    <div class="position-relative">
        {!! Form::number('users_id', null, ['class' => 'form-control']) !!}
    </div>
</div>


<!-- Description Status Field -->
<div class="form-group">
    {!! Form::label('description_status', 'Description Status:') !!}
    <div class="position-relative">
        {!! Form::textarea('description_status', null, ['class' => 'form-control']) !!}
    </div>
</div>


<!-- Status Id Field -->
<div class="form-group">
    {!! Form::label('status_id', 'Status Id:') !!}
    <div class="position-relative">
        {!! Form::number('status_id', null, ['class' => 'form-control']) !!}
    </div>
</div>


<!-- Note Order Field -->
<div class="form-group">
    {!! Form::label('note_order', 'Note Order:') !!}
    <div class="position-relative">
        {!! Form::textarea('note_order', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Submit Field -->
<div class="form-actions center">
    <a href="{!! route('orderProducts.index') !!}" class="btn btn-danger"> <i class="fa fa-close"></i> Batal</a>
    {!! Form::submit('Simpan', ['class' => 'btn btn-green mr-1']) !!}
</div>

