@can('adm.index')
<li class="{{ Request::is('home') ? 'active' : '' }}">
    <a href="{!! route('home') !!}"><i class="fa fa-home"></i><span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title">Home</span></a>
</li>
<li class="{{ Request::is('profil*') ? 'active' : '' }}">
    <a href="{!! route('profil') !!}"><i class="fa fa-user"></i><span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title">Akun</span></a>
</li>
<li class="navigation-header">
    <span data-i18n="nav.category.layouts">--Pemesanan</span>
    <i class="ft-more-horizontal ft-minus" data-toggle="tooltip" data-placement="right" data-original-title="Layouts"></i>
</li>
<li class="{{ Request::is('orderProducts*') ? 'active' : '' }}">
    <a href="{!! route('orderProducts.index') !!}"><i class="fa fa-shopping-basket green"></i><span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title">Order Products</span></a>
</li>
<li class="{{ Request::is('statusOrders*') ? 'active' : '' }}">
    <a href="{!! route('statusOrders.index') !!}"><i class="fa fa-tag"></i><span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title">Status Orders</span></a>
</li>
<li class="navigation-header">
    <span data-i18n="nav.category.layouts">--Interior</span>
    <i class="ft-more-horizontal ft-minus" data-toggle="tooltip" data-placement="right" data-original-title="Layouts"></i>
</li>
<li class="{{ Request::is('interiorGaleris*') ? 'active' : '' }}">
    <a href="{!! route('interiorGaleris.index') !!}"><i class="fa fa-bookmark"></i><span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title">Interior Galeris</span></a>
</li>

<li class="{{ Request::is('categoryInteriors*') ? 'active' : '' }}">
    <a href="{!! route('categoryInteriors.index') !!}"><i class="fa fa-cog"></i><span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title">Kategori Interiors</span></a>
</li>
<li class="navigation-header">
    <span data-i18n="nav.category.layouts">--Produk</span>
    <i class="ft-more-horizontal ft-minus" data-toggle="tooltip" data-placement="right" data-original-title="Layouts"></i>
</li>
<li class="{{ Request::is('produks*') ? 'active' : '' }}">
    <a href="{!! route('produks.index') !!}"><i class="fa fa-bookmark"></i><span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title">Produks</span></a>
</li>
<li class="{{ Request::is('kategoriProduks*') ? 'active' : '' }}">
    <a href="{!! route('kategoriProduks.index') !!}"><i class="fa fa-cog"></i><span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title">Kategori Produks</span></a>
</li>

{{--<li class="{{ Request::is('buktiPembayarans*') ? 'active' : '' }}">--}}
{{--    <a href="{!! route('buktiPembayarans.index') !!}"><i class="icon-circle-right"></i><span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title">Bukti Pembayarans</span></a>--}}
{{--</li>--}}

<li class="navigation-header">
    <span data-i18n="nav.category.layouts">--Info Pemesanan</span>
    <i class="ft-more-horizontal ft-minus" data-toggle="tooltip" data-placement="right" data-original-title="Layouts"></i>
</li>
<li class="{{ Request::is('rekenings*') ? 'active' : '' }}">
    <a href="{!! route('rekenings.index') !!}"><i class="fa fa-credit-card"></i><span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title">Rekenings</span></a>
</li>

<li class="navigation-header">
    <span data-i18n="nav.category.layouts">--Pengaturan</span>
    <i class="ft-more-horizontal ft-minus" data-toggle="tooltip" data-placement="right" data-original-title="Layouts"></i>
</li>
<li class="nav-item"><a href="#"><i class="fa fa-users"></i><span class="menu-title font-small-4 black" data-i18n="nav.dash.main">Pengaturan User</span></a>
    <ul class="menu-content">
        @can('users.index')
        <li class="{{ Request::is('users*') ? 'active' : '' }}">
            <a href="{!! route('users.index') !!}"><i class="icon-circle-right"></i><span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title">Akun Pengguna</span></a>
        </li>
        @endcan
        @can('permissions.index')
        <li class="{{ Request::is('permissions*') ? 'active' : '' }}">
            <a href="{!! route('permissions.index') !!}"><i class="icon-circle-right"></i><span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title">Permissions</span></a>
        </li>
            @endcan
            @can('roles.index')
        <li class="{{ Request::is('roles*') ? 'active' : '' }}">
            <a href="{!! route('roles.index') !!}"><i class="icon-circle-right"></i><span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title">Roles</span></a>
        </li>
        @endcan
    </ul>
</li>
@endcan
{{--<li class="{{ Request::is('orderFavorites*') ? 'active' : '' }}">--}}
{{--    <a href="{!! route('orderFavorites.index') !!}"><i class="icon-circle-right"></i><span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title">Order Favorites</span></a>--}}
{{--</li>--}}
{{--<li class="{{ Request::is('detailOrders*') ? 'active' : '' }}">--}}
{{--    <a href="{!! route('detailOrders.index') !!}"><i class="icon-circle-right"></i><span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title">Detail Orders</span></a>--}}
{{--</li>--}}

