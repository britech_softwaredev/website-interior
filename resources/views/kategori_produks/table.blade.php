<!-- Sudah di modifikasi untuk Edit,Lihat,Hapus -->
<table class="table table-hover table-bordered table-striped default table-responsive">
    <colgroup>
        <col class="col-xs-1">
        <col class="col-xs-7">
    </colgroup>
    <thead>
    <tr>
        <th><code>#</code></th>
        <th>Title</th>
        <th>Title Eng</th>
        <th>Description</th>
        <th>Description Eng</th>
        <th>Slug</th>
        <th style="text-align: center">Action</th>
    </tr>
    </thead>
    <tbody>
    @php
        $no = 1;
    @endphp
    @foreach($kategoriProduks as $kategoriProduk)
        <tr>
            <td>{!! $no++ !!}</td>
            <td>{!! $kategoriProduk->title !!}</td>
            <td>{!! $kategoriProduk->title_eng !!}</td>
            <td>{!! $kategoriProduk->description !!}</td>
            <td>{!! $kategoriProduk->description_eng !!}</td>
            <td>{!! $kategoriProduk->slug !!}</td>
            <td>
                {!! Form::open(['route' => ['kategoriProduks.destroy', $kategoriProduk->id], 'method' => 'delete']) !!}
                <div class="btn-group" role="group" aria-label="Basic example">
                    <a href="{!! route('kategoriProduks.show', [$kategoriProduk->id]) !!}" class="btn btn-sm btn-success"><i class="fa fa-eye"></i></a>
                                       <a href="{!! route('kategoriProduks.edit', [$kategoriProduk->id]) !!}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
                                       {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-sm btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
