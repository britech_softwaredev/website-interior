<!-- Sudah di modifikasi -->
<!-- Title Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('title', 'Title:') !!}
        </h5>
        {!! $kategoriProduk->title !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Title Eng Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('title_eng', 'Title Eng:') !!}
        </h5>
        {!! $kategoriProduk->title_eng !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Description Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('description', 'Description:') !!}
        </h5>
        {!! $kategoriProduk->description !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Description Eng Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('description_eng', 'Description Eng:') !!}
        </h5>
        {!! $kategoriProduk->description_eng !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Slug Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('slug', 'Slug:') !!}
        </h5>
        {!! $kategoriProduk->slug !!}
    </div>
</div>

