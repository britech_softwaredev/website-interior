<div class="row">
    <div class="col-12">
        <div>
            <a data-target="#addProduk" data-toggle="modal" href="javascript:void(0)" class="btn btn-lg btn-success btn-glow col-12">Tambah Produk</a>
            @include('interior_galeris.add_produk_modal')
            <div class="row mt-5">
                @foreach($interiorGaleri['products'] as $item)
                    <div class="col-3">
                        <div class="card">
                            <div class="card-body box-shadow-0-1 rounded-2 border-grey border-lighten-3">
                                <div style="background-image: url('{{ $item->getFirstMediaUrl() }}');background-position: top;background-size: cover;height: 250px"></div>
                                <div class="font-small-4 text-bold-600 black mt-2 mb-1">{{ $item->title }}</div>
                                <h6>{{ $item->color }}</h6>
                                <h6>{{ $item->matrial }}</h6>
                                <h6>{{ $item->dimensions }}</h6>
                                <h6 class="red text-bold-600">Rp. {{ $item->price }}</h6>
                                @if($item->komersil == 0)
                                    <span class="badge badge-warning black"><i class="fa fa-tag"></i> Hanya Untuk Interior Design</span>
                                @else
                                    <span class="badge badge-success black"><i class="fa fa-tag"></i> Dapat Di Beli Satuan</span>
                                @endif
                                <div class="d-flex mt-1">
                                    {!! Form::open(['route' => ['produks.destroy', $item->id], 'method' => 'delete']) !!}
                                    <a href="{!! route('produks.edit', [$item->id]) !!}" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>
                                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-sm btn-red', 'onclick' => "return confirm('Hapus Data Produk?')"]) !!}
                                    {!! Form::close() !!}

                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

