@extends('layouts.app')
@section('content')
    <div class="content-body">
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-md-12">
                    <div class="card overflow-hidden">
                        <div class="card-content">
                            <div class="media align-items-stretch">
                                <div class="bg-blue p-2 media-middle">
                                    <i class="fa fa-align-left font-large-2 text-white"></i>
                                </div>
                                <div class="media-body p-1">
                                    <span class="blue font-medium-5"> Interior Galeri</span><br>
                                    <span style="margin-top: -5px">Detail Interior Galeri</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    @include('adminlte-templates::common.errors')
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body rounded-2">
                                <h4 class="form-section">
                                    <a href="{!! route('interiorGaleris.index') !!}" class="btn btn-icon danger btn-lg pl-0 ml-0"><i class="ft-arrow-left"></i> Kembali</a>
                                </h4>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <img src="{{ url($interiorGaleri->getFirstMediaUrl()) }}" alt="" class="img-fluid rounded-1 box-shadow-1">
                                    </div>
                                    <div class="col-lg-6">
                                        <table class="table table-hover table-bordered ">
                                            <thead>
                                            <tr>
                                                <td>
                                                    <span class="badge badge-info">Kategori : {{ $interiorGaleri['categoryInterior']['title'] }}</span>
                                                    <div class="font-medium-3 text-bold-700 black mb-0-1">{!! $interiorGaleri->title !!}</div>
                                                    <p>{!! $interiorGaleri->description !!}</p>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="font-medium-1 text-bold-600 black mb-0-1">Harga : Rp. {!! $interiorGaleri->price !!}</div>
                                                    <div class="font-medium-1 text-bold-600 black mb-0-1">Dimensi : {!! $interiorGaleri->dimensions !!}</div>
                                                </td>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @include('interior_galeris.show_fields')

                </div>
            </div>
        </section>
    </div>
@endsection
