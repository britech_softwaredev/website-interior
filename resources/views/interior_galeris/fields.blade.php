<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <div class="position-relative">
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>
</div>


{{--<!-- Title Eng Field -->--}}
{{--<div class="form-group">--}}
{{--    {!! Form::label('title_eng', 'Title Eng:') !!}--}}
{{--    <div class="position-relative">--}}
{{--        {!! Form::text('title_eng', null, ['class' => 'form-control']) !!}--}}
{{--    </div>--}}
{{--</div>--}}


<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <div class="position-relative">
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
    </div>
</div>


{{--<!-- Description Eng Field -->--}}
{{--<div class="form-group">--}}
{{--    {!! Form::label('description_eng', 'Description Eng:') !!}--}}
{{--    <div class="position-relative">--}}
{{--        {!! Form::textarea('description_eng', null, ['class' => 'form-control']) !!}--}}
{{--    </div>--}}
{{--</div>--}}


<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <div class="position-relative">
        {!! Form::number('price', null, ['class' => 'form-control']) !!}
    </div>
</div>


<!-- Dimensions Field -->
<div class="form-group">
    {!! Form::label('dimensions', 'Dimensions:') !!}
    <div class="position-relative">
        {!! Form::text('dimensions', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Category Interior Id Field -->
<div class="form-group">
    {!! Form::label('category_interior_id', 'Category Interior Id:') !!}
    <div class="position-relative">
        {!! Form::select('category_interior_id', $categoryInterior,null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('foto', 'Foto :') !!}
    <div class="position-relative">
        @if(!isset($post))
            <x-media-library-attachment  multiple name="media" rules="mimes:png,jpeg,pdf"/>
        @else
            <x-media-library-collection :model="$post"  name="media" rules="mimes:png,jpeg,pdf"/>
        @endif
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Submit Field -->
<div class="form-actions center">
    <a href="{!! route('interiorGaleris.index') !!}" class="btn btn-danger"> <i class="fa fa-close"></i> Batal</a>
    {!! Form::submit('Simpan', ['class' => 'btn btn-green mr-1']) !!}
</div>

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/media-pro/styles.css')}}">

    <livewire:styles />
    <livewire:scripts />

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.6.0/dist/alpine.min.js" defer></script>
@endsection
