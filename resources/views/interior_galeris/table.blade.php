<!-- Sudah di modifikasi untuk Edit,Lihat,Hapus -->
<table class="table table-hover table-bordered table-striped default table-responsive">
    <colgroup>
        <col class="col-xs-1">
        <col class="col-xs-7">
    </colgroup>
    <thead>
    <tr>
        <th><code>#</code></th>
        <th>Title</th>
        <th>Description</th>
        <th>Category Interior</th>
        <th style="text-align: center">Action</th>
    </tr>
    </thead>
    <tbody>
    @php
        $no = 1;
    @endphp
    @foreach($interiorGaleris as $interiorGaleri)
        <tr>
            <td>{!! $no++ !!}</td>
            <td>
                <img src="{{ url($interiorGaleri->getFirstMediaUrl()) }}" alt="" class="img-fluid width-200">
                {!! $interiorGaleri->title !!}
            </td>
            <td><p class="desc-p">{!! $interiorGaleri->description !!}</p></td>
            <td>{!! $interiorGaleri['categoryInterior']['title'] !!}</td>
            <td>
                {!! Form::open(['route' => ['interiorGaleris.destroy', $interiorGaleri->id], 'method' => 'delete']) !!}
                <div class="btn-group" role="group" aria-label="Basic example">
                    <a href="{!! route('interiorGaleris.show', [$interiorGaleri->id]) !!}" class="btn btn-sm btn-success"><i class="fa fa-eye"></i></a>
                                       <a href="{!! route('interiorGaleris.edit', [$interiorGaleri->id]) !!}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
                                       {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-sm btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
