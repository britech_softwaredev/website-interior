<div class="modal fade text-left" id="addProduk" tabindex="-1" role="dialog" aria-labelledby="addProduk"
     aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title font-medium-1 text-text-bold-700 black" id="myModalLabel33">Tambah Data Produk</label>
                <button type="button" class="close danger" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ft-x"></i></span>
                </button>
            </div>
            <div class="card-body">
                {!! Form::open(['route' => 'produks_modal'], ['class' => 'form','files' => true]) !!}
                <input type="text" value="{{ $interiorGaleri->id }}" name="interior" id="interior" hidden>
                    <div class="form-body">
                        <div class="row">
                            <div class="col-6 mt-2">
                                {!! Form::label('title', 'Nama Interior') !!}
                                {!! Form::text('title', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-6 mt-2">
                                {!! Form::label('title_eng', 'Nama Interior (Eng)') !!}
                                {!! Form::text('title_eng', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-6 mt-2">
                                {!! Form::label('description', 'Description') !!}
                                {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-6 mt-2">
                                {!! Form::label('description_eng', 'Description (Eng)') !!}
                                {!! Form::textarea('description_eng', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-6 mt-2">
                                {!! Form::label('price', 'Harga Furniture (Rupiah Rp.)') !!}
                                {!! Form::number('price', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-6 mt-2">
                                {!! Form::label('color', 'Warna Furniture') !!}
                                {!! Form::text('color', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-6 mt-2">
                                {!! Form::label('material', 'Material') !!}
                                {!! Form::text('material', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-6 mt-2">
                                {!! Form::label('dimensions', 'Dimensions (Ukuran Furniture)') !!}
                                {!! Form::text('dimensions', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-6 mt-2">
                                {!! Form::label('produsen', 'Produsen Furniture') !!}
                                {!! Form::text('produsen', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-4 mt-2">
                                {!! Form::label('category_product_id', 'Kategori Product') !!}
                                {!! Form::select('category_product_id',$categoriProduk, null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-2 mt-2">
                                {!! Form::label('komersil', 'Komersial') !!}
                                <select name="komersil" id="komersil" class="form-control">
                                    <option value="1">Ya</option>
                                    <option value="0">Tidak</option>
                                </select>
                            </div>
                            <div class="col-12 mt-2">
                                {!! Form::label('foto', 'Foto Furniture') !!}
                                @if(!isset($produk))
                                    <x-media-library-attachment  multiple name="media" rules="mimes:png,jpeg,pdf"/>
                                @else
                                    <x-media-library-collection :model="$produk"  name="media" rules="mimes:png,jpeg,pdf"/>
                                @endif
                            </div>
                        </div>

                        <!-- Sudah di modifikasi -->
                        <!-- Submit Field -->
                        <div class="form-actions center">
                            <a href="{!! route('produks.index') !!}" class="btn btn-danger"> <i class="fa fa-close"></i> Batal</a>
                            {!! Form::submit('Simpan', ['class' => 'btn btn-green mr-1']) !!}
                        </div>
                        @section('css')
                            <link rel="stylesheet" type="text/css" href="{{ asset('css/media-pro/styles.css')}}">

                            <livewire:styles />
                            <livewire:scripts />

                            <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.6.0/dist/alpine.min.js" defer></script>
                        @endsection

                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
