<!-- Sudah di modifikasi untuk Edit,Lihat,Hapus -->
<table class="table table-hover table-bordered table-striped default table-responsive">
    <colgroup>
        <col class="col-xs-1">
        <col class="col-xs-7">
    </colgroup>
    <thead>
    <tr>
        <th><code>#</code></th>
        <th>Deskripsi</th>
        <th>Order Product Id</th>
        <th style="text-align: center">Action</th>
    </tr>
    </thead>
    <tbody>
    @php
        $no = 1;
    @endphp
    @foreach($buktiPembayarans as $buktiPembayaran)
        <tr>
            <td>{!! $no++ !!}</td>
            <td>{!! $buktiPembayaran->deskripsi !!}</td>
            <td>{!! $buktiPembayaran->order_product_id !!}</td>
            <td>
                {!! Form::open(['route' => ['buktiPembayarans.destroy', $buktiPembayaran->id], 'method' => 'delete']) !!}
                <div class="btn-group" role="group" aria-label="Basic example">
                    <a href="{!! route('buktiPembayarans.show', [$buktiPembayaran->id]) !!}" class="btn btn-sm btn-success"><i class="fa fa-eye"></i></a>
                                       <a href="{!! route('buktiPembayarans.edit', [$buktiPembayaran->id]) !!}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
                                       {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-sm btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
