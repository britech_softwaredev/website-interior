@extends('layouts.app')
@section('content')
    <div class="content-body">
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-12">
                <div class="card box-shadow-1 rounded-1">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="media d-flex">
                                <div class="align-self-center">
                                    <i class="icon-note info font-large-2 float-left"></i>
                                </div>
                                <div class="media-body text-right">
                                    <h1>{{ \App\Models\Produk::count() }}</h1>
                                    <span>PRODUK TERINPUT</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-12">
                <div class="card box-shadow-1 rounded-1">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="media d-flex">
                                <div class="align-self-center">
                                    <i class="icon-layers success font-large-2 float-left"></i>
                                </div>
                                <div class="media-body text-right">
                                    <h1>{{ \App\Models\InteriorGaleri::count() }}</h1>
                                    <span>DESIGN INTERIOR</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-12">
                <div class="card box-shadow-1 rounded-1">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="media d-flex">
                                <div class="align-self-center">
                                    <i class="icon-handbag danger font-large-2 float-left"></i>
                                </div>
                                <div class="media-body text-right">
                                    <h1>{{ \App\Models\OrderProduct::count() }}</h1>
                                    <span>ORDER FURNITUR</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-12">
                <div class="card box-shadow-1 rounded-1">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="media d-flex">
                                <div class="align-self-center">
                                    <i class="icon-user-following warning font-large-2 float-left"></i>
                                </div>
                                <div class="media-body text-right">
                                    <h1>{{ \App\Models\User::count() }}</h1>
                                    <span>PENGGUNA APLIKASI</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
