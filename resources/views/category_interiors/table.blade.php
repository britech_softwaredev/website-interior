<!-- Sudah di modifikasi untuk Edit,Lihat,Hapus -->
<table class="table table-hover table-bordered table-striped default table-responsive">
    <colgroup>
        <col class="col-xs-1">
        <col class="col-xs-7">
    </colgroup>
    <thead>
    <tr>
        <th><code>#</code></th>
        <th>Title</th>
        <th>Title Eng</th>
        <th>Description</th>
        <th>Description Eng</th>
        <th>Slug</th>
        <th style="text-align: center">Action</th>
    </tr>
    </thead>
    <tbody>
    @php
        $no = 1;
    @endphp
    @foreach($categoryInteriors as $categoryInterior)
        <tr>
            <td>{!! $no++ !!}</td>
            <td>{!! $categoryInterior->title !!}</td>
            <td>{!! $categoryInterior->title_eng !!}</td>
            <td>{!! $categoryInterior->description !!}</td>
            <td>{!! $categoryInterior->description_eng !!}</td>
            <td>{!! $categoryInterior->slug !!}</td>
            <td>
                {!! Form::open(['route' => ['categoryInteriors.destroy', $categoryInterior->id], 'method' => 'delete']) !!}
                <div class="btn-group" role="group" aria-label="Basic example">
                    <a href="{!! route('categoryInteriors.show', [$categoryInterior->id]) !!}" class="btn btn-sm btn-success"><i class="fa fa-eye"></i></a>
                                       <a href="{!! route('categoryInteriors.edit', [$categoryInterior->id]) !!}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
                                       {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-sm btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
