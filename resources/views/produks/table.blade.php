<!-- Sudah di modifikasi untuk Edit,Lihat,Hapus -->
<table class="table table-hover table-bordered default">
    <colgroup>
        <col class="col-xs-1">
        <col class="col-xs-7">
    </colgroup>
    <thead>
    <tr>
        <th><code>#</code></th>
        <th>Title</th>
        <th>Description</th>
        <th>Speksifikasi</th>
        <th>Category</th>
        <th>Komersil</th>
        <th style="text-align: center">Action</th>
    </tr>
    </thead>
    <tbody>
    @php
        $no = 1;
    @endphp
    @foreach($produks as $produk)
        <tr>
            <td>{!! $no++ !!}</td>
            <td>
                <img src="{{ url($produk->getFirstMediaUrl()) }}" alt="" class="img-fluid width-100"><br>
                {!! $produk->title !!}</td>
            <td><p class="desc-p">{!! $produk->description !!}</p></td>
            <td>
                - Harga : {!! $produk->price !!}<br>
                - Warna : {!! $produk->color !!}<br>
                - Material : {!! $produk->material !!}<br>
                - Dimensi : {!! $produk->dimensions !!}<br>
                - Produsen : {!! $produk->produsen !!}</td>
            <td>{!! $produk['categoryProduct']['title'] !!}</td>
            <td>
                @if($produk->komersil == 0)
                    <span class="badge badge-warning black"><i class="fa fa-tag"></i> Interior Only</span>
                @else
                    <span class="badge badge-success black"><i class="fa fa-tag"></i> Publish</span>
                @endif
            </td>
            <td>
                {!! Form::open(['route' => ['produks.destroy', $produk->id], 'method' => 'delete']) !!}
                <div class="btn-group" role="group" aria-label="Basic example">
                    <a href="{!! route('produks.show', [$produk->id]) !!}" class="btn btn-sm btn-success"><i class="fa fa-eye"></i></a>
                                       <a href="{!! route('produks.edit', [$produk->id]) !!}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
                                       {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-sm btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
