<!-- Sudah di modifikasi -->
<!-- Title Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('title', 'Title:') !!}
        </h5>
        {!! $produk->title !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Title Eng Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('title_eng', 'Title Eng:') !!}
        </h5>
        {!! $produk->title_eng !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Description Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('description', 'Description:') !!}
        </h5>
        {!! $produk->description !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Description Eng Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('description_eng', 'Description Eng:') !!}
        </h5>
        {!! $produk->description_eng !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Price Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('price', 'Price:') !!}
        </h5>
        {!! $produk->price !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Color Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('color', 'Color:') !!}
        </h5>
        {!! $produk->color !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Material Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('material', 'Material:') !!}
        </h5>
        {!! $produk->material !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Dimensions Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('dimensions', 'Dimensions:') !!}
        </h5>
        {!! $produk->dimensions !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Produsen Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('produsen', 'Produsen:') !!}
        </h5>
        {!! $produk->produsen !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Slug Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('slug', 'Slug:') !!}
        </h5>
        {!! $produk->slug !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Category Product Id Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('category_product_id', 'Category Product Id:') !!}
        </h5>
        {!! $produk->category_product_id !!}
    </div>
</div>

