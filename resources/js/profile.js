import Vue from 'vue'

const app = new Vue({
    el: '#profile',
    data: {
        selected: null,
        value:'',
        file:'',
    },
    methods: {
        select: function(result) {
            this.value = null;
            this.file = null;
            this.selected = result;
        },
        input: function () {
            this.selected = null;
        },
    }
});
