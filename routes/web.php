<?php

use App\Http\Controllers\BuktiPembayaranController;
use App\Http\Controllers\CategoryInteriorController;
use App\Http\Controllers\DetailOrderController;
use App\Http\Controllers\InteriorGaleriController;
use App\Http\Controllers\InteriorHasProductController;
use App\Http\Controllers\KategoriProdukController;
use App\Http\Controllers\OrderFavoritesController;
use App\Http\Controllers\OrderProductController;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RekeningController;
use App\Http\Controllers\StatusOrderController;
use App\Http\Controllers\WebsiteFrontEnd\WebsiteController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//Route::get('/', function () {
//   return view('welcome');
//});

Auth::routes(['verify' => true,
    'register' => true,
    'reset' => false,
]);

Route::get('interior_galery', function(){
    return view('website.interior_galery.index');
});

Route::get('interior_galery/detail', function(){
    return view('website.interior_galery.detail');
});
Route::get('interior_galery/detail2', function(){
    return view('website.interior_galery.detail2');
});
Route::get('interior_galery/detail3', function(){
    return view('website.interior_galery.detail3');
});
Route::get('interior_galery/detail4', function(){
    return view('website.interior_galery.detail4');
});
Route::get('interior_galery/detail5', function(){
    return view('website.interior_galery.detail5');
});

Route::get('interior_galery/detail6', function(){
    return view('website.interior_galery.detail6');
});

//Route::get('product_interior', function(){
//    return view('website.product_interior.index');
//});

Route::get('/refereshcaptcha', 'App\Http\Controllers\Auth\LoginController@refereshCaptcha');

//Custom Auth Routes
// Authentication Routes...

// Password Reset Routes...
/*$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');*/

Route::group(['middleware' => ['auth']], function () {

    Route::mediaLibrary();

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::resource('roles', App\Http\Controllers\RoleController::class);

    Route::resource('permissions', App\Http\Controllers\PermissionController::class);

    Route::resource('users', App\Http\Controllers\UserController::class);

    Route::get('profil', [App\Http\Controllers\UserController::class, 'profil'])->name('profil');
    Route::get('editProfil/{id}', [App\Http\Controllers\UserController::class, 'editProfiles']);
    Route::patch('updateProfile/{id}', [App\Http\Controllers\UserController::class, 'updateProfile']);
    Route::resource('profiles', 'App\Http\Controllers\ProfileController');
    Route::get('editProfiles/{id}', [ProfileController::class,'editInfo']);

//
//    Route::resource('galeris', App\Http\Controllers\GaleriController::class);
//
//    Route::resource('pages', 'App\Http\Controllers\PageController');
//    Route::post('pagesIncrease/{id}', 'App\Http\Controllers\PageController@increase')->name('pagesIncrease');
//    Route::post('pagesDecrease/{id}', 'App\Http\Controllers\PageController@decrease')->name('pagesDecrease');
//
//    Route::resource('pageCategories', 'App\Http\Controllers\PageCategoryController');
//    Route::post('pageCategoriesIncrease/{id}', 'App\Http\Controllers\PageCategoryController@increase')->name('pageCategoriesIncrease');
//    Route::post('pageCategoriesDecrease/{id}', 'App\Http\Controllers\PageCategoryController@decrease')->name('pageCategoriesDecrease');
//
//    Route::resource('posts', 'App\Http\Controllers\PostController');
//
//    Route::resource('postCategories', 'App\Http\Controllers\PostCategoryController');
//    Route::post('postCategoriesIncrease/{id}', 'App\Http\Controllers\PostCategoryController@increase')->name('postCategoriesIncrease');
//    Route::post('postCategoriesDecrease/{id}', 'App\Http\Controllers\PostCategoryController@decrease')->name('postCategoriesDecrease');
    Route::resource('interiorGaleris', InteriorGaleriController::class);
    Route::resource('categoryInteriors', CategoryInteriorController::class);
    Route::resource('kategoriProduks', KategoriProdukController::class);
    Route::resource('produks', ProdukController::class);
    Route::post('produks_modal', [ProdukController::class, 'storeModal'])->name('produks_modal');
    Route::resource('interiorHasProducts', InteriorHasProductController::class);

    Route::resource('statusOrders', StatusOrderController::class);
    Route::resource('orderProducts', OrderProductController::class);
    Route::put('postOrder/{orderId}', [OrderProductController::class,'postOrder']);
    Route::get('updateStatus/{idOrder}', [OrderProductController::class,'updateStatus']);
    Route::resource('detailOrders', DetailOrderController::class);
    Route::resource('orderFavorites', OrderFavoritesController::class);
    Route::get('favoritesAdd/{interiorId}', [OrderFavoritesController::class,'favoriteAdd']);
    Route::get('order/{productId}', [OrderProductController::class,'insertOrder']);
    Route::resource('rekenings', RekeningController::class);
    Route::resource('buktiPembayarans', BuktiPembayaranController::class);
});

//----WEBSITE FRONT END
//Route::get('/', [\App\Http\Controllers\WebsiteFrontEnd\WebsiteController::class,'index'])->name('beranda');
//
////Detail Post
Route::get('post/{slug}', [\App\Http\Controllers\WebsiteFrontEnd\WebsiteController::class,'detailPost'])->name('detail-post');
Route::get('page/{slug}', [\App\Http\Controllers\WebsiteFrontEnd\WebsiteController::class,'detailPage'])->name('detail-page');
//
////Post Category
//Route::get('category/{slug}', [\App\Http\Controllers\WebsiteFrontEnd\WebsiteController::class,'listPostByCategory'])->name('category-post');
//Route::get('sejarah', [\App\Http\Controllers\WebsiteFrontEnd\WebsiteController::class,'profilSejarah']);
//Route::get('profil-anggota-dprd', [\App\Http\Controllers\WebsiteFrontEnd\WebsiteController::class,'profilAnggotaDprd']);
//Route::get('post-utama', [\App\Http\Controllers\WebsiteFrontEnd\WebsiteController::class,'beritaUtama']);
//Route::get('post-utama/indeks', [\App\Http\Controllers\WebsiteFrontEnd\WebsiteController::class,'beritaSearch']);
//Route::get('anggota-dewan', [\App\Http\Controllers\WebsiteFrontEnd\WebsiteController::class,'anggotaDewan']);
//Route::get('alat-kelengkapan-dewan/{slugAkdCategory}', [\App\Http\Controllers\WebsiteFrontEnd\WebPersonController::class,'akdListPerson'])->name('alat-kelengkapan-dewan');
//Route::get('semua-anggota-dewan', [\App\Http\Controllers\WebsiteFrontEnd\WebPersonController::class,'listPersonDewan'])->name('semua-anggota-dewan');
//Route::get('semua-anggota-sekretariat', [\App\Http\Controllers\WebsiteFrontEnd\WebPersonController::class,'listPersonSekretariat'])->name('semua-anggota-sekretariat');
//Route::get('galeri-foto', [\App\Http\Controllers\WebsiteFrontEnd\WebsiteController::class,'galeriFoto'])->name('galeri-foto');
//Route::get('galeri-foto/{slug}', [\App\Http\Controllers\WebsiteFrontEnd\WebsiteController::class,'detailGaleriFoto'])->name('detail-galeri-foto');
//Route::get('detail-person-dewan/{slug}', [\App\Http\Controllers\WebsiteFrontEnd\WebPersonController::class,'detailPersonDewan'])->name('detail-person-dewan');
//Route::get('detail-person-sekretariat/{slug}', [\App\Http\Controllers\WebsiteFrontEnd\WebPersonController::class,'detailPersonSekretariat'])->name('detail-person-sekretariat');
//Route::get('detail-instagram/{slug}', [\App\Http\Controllers\WebsiteFrontEnd\WebsiteController::class,'detailInstagram'])->name('detail-instagram');
//Route::get('agenda-dprd', [WebsiteController::class,'agendaDprd'])->name('agenda-dprd');
//Route::get('agenda/agenda-detail/{slug}', [WebsiteController::class,'detailAgenda'])->name('agenda-detail');

Route::get('/',[WebsiteController::class,'index'])->name('beranda');
Route::get('interior',[WebsiteController::class,'interiorGaleri'])->name("interior-galeri");
Route::get('interior/{slug}',[WebsiteController::class,'detailInterior'])->name("interior-detail");
Route::get('product',[WebsiteController::class,'product'])->name('product-interior');
Route::get('checkout', [WebsiteController::class,'checkout']);
Route::get('orderStatus', [WebsiteController::class,'statusOrder'])->name('order_status');
Route::get('favorite', [WebsiteController::class,'favoriteInterior'])->name('favorite');
