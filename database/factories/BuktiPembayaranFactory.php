<?php

namespace Database\Factories;

use App\Models\BuktiPembayaran;
use Illuminate\Database\Eloquent\Factories\Factory;

class BuktiPembayaranFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BuktiPembayaran::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'deskripsi' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s'),
        'order_product_id' => $this->faker->randomDigitNotNull
        ];
    }
}
