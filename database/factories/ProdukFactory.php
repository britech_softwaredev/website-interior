<?php

namespace Database\Factories;

use App\Models\Produk;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProdukFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Produk::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->word,
        'title_eng' => $this->faker->word,
        'description' => $this->faker->text,
        'description_eng' => $this->faker->text,
        'price' => $this->faker->word,
        'color' => $this->faker->word,
        'material' => $this->faker->word,
        'dimensions' => $this->faker->word,
        'produsen' => $this->faker->word,
        'slug' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s'),
        'category_product_id' => $this->faker->randomDigitNotNull
        ];
    }
}
