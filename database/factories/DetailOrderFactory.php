<?php

namespace Database\Factories;

use App\Models\DetailOrder;
use Illuminate\Database\Eloquent\Factories\Factory;

class DetailOrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DetailOrder::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s'),
        'product_id' => $this->faker->word,
        'order_product_id' => $this->faker->randomDigitNotNull
        ];
    }
}
