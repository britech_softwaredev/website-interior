<?php

namespace Database\Factories;

use App\Models\Jdih;
use Illuminate\Database\Eloquent\Factories\Factory;

class JdihFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Jdih::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
        'definition' => $this->faker->text,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s'),
        'slug' => $this->faker->word,
        'tahun' => $this->faker->randomDigitNotNull,
        'download_count' => $this->faker->randomDigitNotNull,
        'jdih_category_id' => $this->faker->word,
        'subject' => $this->faker->word,
        'file' => $this->faker->word,
        'jdih_status_id' => $this->faker->word
        ];
    }
}
