<?php

namespace Database\Factories;

use App\Models\OrderFavorites;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFavoritesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = OrderFavorites::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'users_id' => $this->faker->word,
        'uuid' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'product_id' => $this->faker->word,
        'interior_id' => $this->faker->word
        ];
    }
}
