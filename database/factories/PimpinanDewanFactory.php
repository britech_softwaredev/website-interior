<?php

namespace Database\Factories;

use App\Models\PimpinanDewan;
use Illuminate\Database\Eloquent\Factories\Factory;

class PimpinanDewanFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PimpinanDewan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'jabatan_dewan_id' => $this->faker->word,
        'order' => $this->faker->randomDigitNotNull
        ];
    }
}
