<?php

namespace Database\Factories;

use App\Models\InteriorGaleri;
use Illuminate\Database\Eloquent\Factories\Factory;

class InteriorGaleriFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = InteriorGaleri::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->word,
        'title_eng' => $this->faker->word,
        'description' => $this->faker->text,
        'description_eng' => $this->faker->text,
        'price' => $this->faker->word,
        'dimensions' => $this->faker->word,
        'slug' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s'),
        'category_interior_id' => $this->faker->randomDigitNotNull
        ];
    }
}
