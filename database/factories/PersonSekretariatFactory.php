<?php

namespace Database\Factories;

use App\Models\PersonSekretariat;
use Illuminate\Database\Eloquent\Factories\Factory;

class PersonSekretariatFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PersonSekretariat::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
        'date_of_birth' => $this->faker->word,
        'place_of_birth' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s'),
        'address' => $this->faker->word,
        'pendidikan_id' => $this->faker->word,
        'slug' => $this->faker->word,
        'jabatan_sekretariat_id' => $this->faker->word,
        'foto' => $this->faker->word
        ];
    }
}
